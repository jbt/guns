#ifndef PLAYER_HPP_INCLUDED
#define PLAYER_HPP_INCLUDED

#include "Badge.hpp"
#include "Animation.hpp"
#include "Calc.hpp"
#include <boost/asio.hpp>
#include <boost/concept_check.hpp>
#include <functional>

struct Move;
class Game;

class Player : public std::enable_shared_from_this<Player>
{
public:
    enum class Action
    {
        CONNECT,
        DISCONNECT,
        DUCK,
        NAME,
        JOIN,
        LOAD,
        SAY,
        SHOOT,
        TELL,
        WHO
    };
    Game& game_;
    std::string name_;
    int ammo_;
    bool ducked_;
    int duck_count_;
    std::string argument_;
    bool color_, utf8_;
    typedef std::function<bool(std::shared_ptr<Player>)> Cantrip;
    Cantrip cantrip_;
    std::vector<std::string> command_arguments_;
    std::string password_;
    bool verbose_;
    int rating_;
    std::vector<Calc> calcs_;
    int shots_taken_;

    Player( Game& game, bool verbose );
    virtual ~Player();
    virtual void RoundBegins();
    virtual void GameBegins();
    virtual void NotifyOfMove( const Move& m );
    virtual void Listen(boost::asio::ip::tcp::acceptor& acceptor) = 0;
    virtual boost::asio::ip::address Address() const = 0;
    virtual bool Connected() const = 0;
    virtual bool Idle() const;
    virtual bool Inactive() const;
    virtual void Disconnect() = 0;
    virtual void Say( std::string message ) = 0;
    virtual void Prompt( const std::string& text, std::function<void(const std::string&)> cb );
    virtual void Become( const Player& src );
    virtual void DeltaLuck( int d ) = 0;
    virtual void Win();
    virtual void Lose();
    virtual void ScoreKill();
    virtual void EarnBadge( Badge b );
    virtual void DisplayAnimation(ani::Mation& v);
    int Wins() const;
    int Losses() const;
    int Kills() const;
    int Luck() const;
    bool operator==(const Player& rhs) const;
    bool MoreActive(const Player& p) const;
    void Touch();
    float WL() const;
    void CapRating();
protected:

    boost::posix_time::ptime last_active_;
    int wins_, losses_, kills_, luck_;

    const char* Help( const std::vector<std::string>& args ) const;
    void Wins( int val );
    void Losses( int val );
    void Kills( int val );
    void Luck( int val );
};

#endif
