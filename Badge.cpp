#include "Badge.hpp"
#include "Color.hpp"
#include <boost/concept_check.hpp>
#include <iostream>
#include <tuple>
#include <stdexcept>

std::ostream& operator<<(std::ostream& o, Badge b)
{
    o << 'b';
    o << static_cast<int>(b);
    return o;
}

std::istream& operator>>(std::istream& i, Badge& b)
{
    char c;
    i >> c;
    if ( c != 'b' )
    {
        throw std::runtime_error("Couldn't extract badge.");
    }
    int j = 0;
    i >> j;
    b = static_cast<Badge>( j );
    return i;
}

namespace {
    std::tuple<const char*,const char*,const char*> Text(Badge b)
    {
        switch ( b )
        {
            case Badge::DISCRETION_IS_THE_WORST_PART_OF_VALOR:
                return std::make_tuple("Discretion is the Worst Part of Valor",
                                       "𝘚𝘰𝘮𝘦𝘵𝘪𝘮𝘦𝘴 𝘤𝘰𝘸𝘢𝘳𝘥𝘪𝘤𝘦 𝘫𝘶𝘴𝘵 𝘥𝘰𝘦𝘴𝘯'𝘵 𝘱𝘢𝘺.",
                                       "died in round 1, while ducked.");
            case Badge::THE_LION_AND_THE_TIN_MAN:
                return std::make_tuple("The Lion and the Tin Man",
                                       "𝘔𝘢𝘺𝘣𝘦 𝘵𝘩𝘦𝘺 𝘴𝘩𝘰𝘶𝘭𝘥'𝘷𝘦 𝘢𝘴𝘬𝘦𝘥 𝘧𝘰𝘳 𝘢 𝘣𝘳𝘢𝘪𝘯, 𝘵𝘰𝘰.",
                                       "has tied for second place in a game won by a Strawman.");
            case Badge::LUCK_O_THE_LOSERS:
                return std::make_tuple("Luck o' the Losers",
                                       "𝘋𝘦𝘧𝘪𝘯𝘦 '𝘭𝘶𝘤𝘬𝘺'.",
                                       "has achieved a luck modifier greater than 50.");
            case Badge::READY_FOR_THE_ZOMBIES:
                return std::make_tuple("Ready for the Zombies",
                                       "𝘐 𝘥𝘰𝘯'𝘵 𝘸𝘢𝘯𝘵 𝘵𝘰 𝘯𝘪𝘵𝘱𝘪𝘤𝘬, 𝘛𝘰𝘮, 𝘣𝘶𝘵 𝘪𝘴 𝘵𝘩𝘪𝘴 𝘳𝘦𝘢𝘭𝘭𝘺 𝘺𝘰𝘶𝘳 𝘱𝘭𝘢𝘯?\r\n"
                                       "𝘚𝘱𝘦𝘯𝘥 𝘺𝘰𝘶𝘳 𝘸𝘩𝘰𝘭𝘦 𝘭𝘪𝘧𝘦 𝘭𝘰𝘤𝘬𝘦𝘥 𝘪𝘯𝘴𝘪𝘥𝘦 𝘢 𝘮𝘢𝘭𝘭?\r\n"
                                       "𝘔𝘢𝘺𝘣𝘦 𝘵𝘩𝘢𝘵'𝘴 𝘖𝘒 𝘧𝘰𝘳 𝘯𝘰𝘸 𝘣𝘶𝘵 𝘴𝘰𝘮𝘦𝘥𝘢𝘺 𝘺𝘰𝘶'𝘭𝘭 𝘣𝘦 𝘰𝘶𝘵 𝘰𝘧 𝘧𝘰𝘰𝘥 𝘢𝘯𝘥 𝘨𝘶𝘯𝘴,\r\n"
                                       " - Jonathan Coulton, 𝘙𝘦: 𝘠𝘰𝘶𝘳 𝘉𝘳𝘢𝘪𝘯𝘴",
                                       "has had over 50 bullets loaded at once.");
            case Badge::THE_BRAVE_AND_THE_PERSISTENT:
                return std::make_tuple("The Brave and The Persistent",
                                       "𝘔𝘢𝘺𝘣𝘦 𝘵𝘩𝘪𝘴 𝘨𝘢𝘮𝘦 𝘪𝘴 𝘫𝘶𝘴𝘵 𝘯𝘰𝘵 𝘧𝘰𝘳 𝘺𝘰𝘶.",
                                       "has lost over one thousand games.");
            case Badge::DUCK_DUCK_GOO_NO_DUCK:
                return std::make_tuple("Duck, Duck, Goo... No, Duck",
                                       "𝘕𝘰𝘵𝘩𝘪𝘯𝘨 𝘴𝘢𝘺𝘴 \"𝘧𝘪𝘨𝘩𝘵 𝘵𝘰 𝘵𝘩𝘦 𝘥𝘦𝘢𝘵𝘩\" 𝘲𝘶𝘪𝘵𝘦 𝘭𝘪𝘬𝘦 𝘸𝘢𝘵𝘦𝘳 𝘧𝘰𝘸𝘭.",
                                       "has ducked over fifty times in the same game.");
            case Badge::THE_VETERAN:
                return std::make_tuple("The Veteran",
                                       "𝘠𝘰𝘶 𝘥𝘢 𝘮𝘢𝘯!",
                                       "has won over one hundred games.");
            case Badge::THE_SURVIVOR:
                return std::make_tuple("The Survivor",
                                       "𝘖𝘶𝘵𝘸𝘪𝘵, 𝘖𝘶𝘵𝘱𝘭𝘢𝘺, 𝘖𝘶𝘵𝘥𝘶𝘤𝘬",
                                       "has won a game that lasted for more than 50 rounds.");
            case Badge::SPRING_CLEANING:
                return std::make_tuple("Spring Cleaning",
                                       "𝘠𝘰𝘶 𝘬𝘦𝘦𝘱 𝘦𝘹𝘱𝘦𝘤𝘵𝘪𝘯𝘨 𝘱𝘦𝘰𝘱𝘭𝘦 𝘵𝘰 𝘨𝘳𝘰𝘸 𝘰𝘶𝘵 𝘰𝘧 𝘵𝘩𝘪𝘴 𝘴𝘰𝘳𝘵 𝘰𝘧 𝘪𝘮𝘮𝘢𝘵𝘶𝘳𝘪𝘵𝘺, 𝘥𝘰𝘯'𝘵 𝘺𝘰𝘶?",
                                       "has aided another player in coming out, by using ventriloquism to cause them to admit homosexuality." );
            case Badge::THE_PACIFIST:
                return std::make_tuple("The Pacifist",
                                       "𝘛𝘩𝘰𝘴𝘦 𝘸𝘩𝘰 𝘭𝘪𝘷𝘦 𝘣𝘺 𝘵𝘩𝘦 𝘴𝘸𝘰𝘳𝘥 𝘥𝘪𝘦 𝘣𝘺 𝘦𝘢𝘤𝘩 𝘰𝘵𝘩𝘦𝘳'𝘴 𝘨𝘶𝘯𝘴.",
                                       "has won a game without taking a shot.");
            case Badge::SWISS_CHEESE:
                return std::make_tuple("Swiss Cheese",
                                       "𝘠𝘰𝘶 𝘴𝘶𝘳𝘦 𝘩𝘢𝘷𝘦 𝘢 𝘭𝘰𝘵 𝘰𝘧 𝘧𝘳𝘪𝘦𝘯𝘥𝘴, 𝘥𝘰𝘯'𝘵 𝘺𝘢 𝘤𝘩𝘢𝘮𝘱?",
                                       "was hit by more than three bullets simultaneously.");
            case Badge::EMIL_BLEEHALL:
                return std::make_tuple("Emil Bleehal",
                                       "𝘎𝘰𝘰𝘥 𝘭𝘶𝘤𝘬 𝘸𝘪𝘵𝘩 𝘵𝘩𝘢𝘵 𝘕𝘶𝘤𝘭𝘦𝘢𝘳 𝘌𝘯𝘨𝘪𝘯𝘦𝘦𝘳𝘪𝘯𝘨 𝘣𝘢𝘥𝘨𝘦.",
                                       "earned every badge available in the game at the time of awarding.");
            case Badge::NO_BADGE:
            default:
                return std::make_tuple("NoSuchBadge","This is a","BUG");
        }
    }
}
std::string DisplayText(Badge b)
{
    auto t = Text(b);
    return color::RED + std::get<0>(t) + "\r\n"
         + color::NONE + std::get<1>(t) + "\r\n"
         + color::BLUE + "The bearer of this badge " + std::get<2>(t)
         + color::NONE;
}

std::size_t BadgeCount()
{
    return static_cast<size_t>(Badge::BADGE_MAX) - static_cast<size_t>(Badge::NO_BADGE) - 1;
}
