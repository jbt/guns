#include "Move.hpp"
#include "Game.hpp"

bool Move::operator<(const Move& rhs) const
{
    if ( what_ == Player::Action::SHOOT && rhs.what_ != Player::Action::SHOOT )
    {
        return false;
    }
    if ( what_ != Player::Action::SHOOT && rhs.what_ == Player::Action::SHOOT )
    {
        return true;
    }
    if ( who_->name_ < rhs.who_->name_ )
        return true;
    if ( who_->name_ > rhs.who_->name_ )
        return false;
    return target_ < rhs.target_;
}
