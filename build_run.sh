#!/bin/bash
export PATH+=":${HOME}/.local/bin:${HOME}/.local/scr:${HOME}/scr:."
if [ $# -eq 0 ]; then
    $0 $(date) >${HOME}/.local/var/log/guns.log 2>&1 &
else
    cd $(dirname $0)  || exit
    /usr/bin/git fetch || exit
    /usr/bin/git reset --hard origin/master || exit
    /bin/fuser ./bin/guns | xargs kill
    /usr/bin/make || exit
    if [ ./bin/guns -nt ./guns.running ]
    then
        /bin/fuser ./guns.running | xargs kill
	kill.sh guns
        cp ./bin/guns ./guns.running
    fi
    export LD_LIBRARY_PATH='/home/john/.local/lib:/usr/local/lib64:/usr/lib64:/lib64:/usr/local/lib:/usr/lib:/lib:.'
    ./guns.running --verbose
fi

