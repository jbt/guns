#include "Player.hpp"
#include "Badge.hpp"
#include <map>
#include <set>

class Human : public Player
{
public:
    Human( Game& game, boost::asio::io_service& service, bool verbose  );
private:
    static std::map<std::string,std::function<void(Human*,const std::string&)> > ReadDatum();
    boost::asio::ip::tcp::socket socket_;
    boost::asio::streambuf buffer_;
    std::function<void(const std::string& response)> prompt_callback_;
    std::set<Badge> badges_;
    std::string delayed_output_;
    ani::Mation::const_iterator frame_b_, frame_e_;
    boost::asio::deadline_timer animation_timer_;

    virtual boost::asio::ip::address Address() const override;
    virtual void Listen(boost::asio::ip::tcp::acceptor& acceptor) override;
    virtual bool Connected() const override;
    virtual void Disconnect() override;
    virtual void Say(std::string message) override;
    virtual void Become( const Player& src ) override;
    virtual void DeltaLuck( int d ) override;
    virtual void Prompt( const std::string& text, std::function<void(const std::string&)> cb ) override;
    virtual void Win() override;
    virtual void Lose() override;
    virtual void ScoreKill() override;
    virtual void EarnBadge( Badge b ) override;
    virtual void RoundBegins() override;
    virtual void DisplayAnimation(ani::Mation& v) override;

    void HandleConnection( const boost::system::error_code& ec );
    void WaitForName( const boost::system::error_code& ec );
    void WaitForCommand( const boost::system::error_code& ec );
    void GetName( const boost::system::error_code& ec, const size_t& bytes );
    void ParseCommand( const boost::system::error_code& ec, const size_t& bytes );
    void Said( const boost::system::error_code& ec, const size_t& bytes );
    void Save();
    bool Load();
    void Login();
    void CheckPassword( std::string entered_pw );
    void ShowFrame();
};
