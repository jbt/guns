#include "Human.hpp"
#include "Game.hpp"
#include "Color.hpp"
#include <boost/algorithm/string.hpp>
#include <boost/regex.hpp>
#include <boost/asio.hpp>
#include <boost/filesystem/operations.hpp>
#include <fstream>

namespace ph = std::placeholders;
typedef boost::system::error_code err;

namespace {
    void WriteChar( char c )
    {
        if ( std::isgraph(c) )
        {
            std::cout.put(c);
        }
        else
        {
            std::cout << '[' << std::hex << static_cast<short>(static_cast<uint8_t>(c)) << ']';
        }
    }
//     const boost::regex ("[\r\n]+");
    typedef boost::asio::buffers_iterator<boost::asio::streambuf::const_buffers_type> AsIt;
    std::pair<AsIt,bool> line_ending( AsIt b, AsIt e )
    {
        char le[2] = { '\r', '\n' };
        auto it = std::find_first_of( b, e, std::begin(le), std::end(le) );
        if ( it == e )
        {
            std::cout << "No line ending found in (";
            std::for_each( b, e, &WriteChar );
            std::cout << ").\n";
            return std::make_pair( b, false );
        }
        it = std::find_if( it, e, [](char c){return c!='\r'&&c!='\n';} );
        std::cout << "Found complete line (";
        std::for_each( b, it, &WriteChar );
        std::cout << ") leaving behind " << std::distance( it, e )
                << " bytes (";
        std::for_each( it, e, &WriteChar );
        std::cout << ").\n";
        return std::make_pair( it, true );
    }
}

Human::Human( Game& game, boost::asio::io_service& service, bool verbose )
: Player( game, verbose ),
    socket_( service ), buffer_(256),
    frame_b_(ani::SHOT().end()), frame_e_(ani::SHOT().end()),
    animation_timer_( service )
{}

void Human::Listen(boost::asio::ip::tcp::acceptor& acceptor)
{
    if ( verbose_ ) std::cout << __PRETTY_FUNCTION__ << '=' << (void*)this << '\n';
    name_.assign( "UnknownPlayer" );
    ammo_ = 1;
    ducked_ = false;
    duck_count_ = 0;
    argument_.clear();
    acceptor.async_accept( socket_,
                           std::bind(&Human::HandleConnection,
                                     this, ph::_1) );
    Touch();
}

boost::asio::ip::address Human::Address() const
{
    return socket_.remote_endpoint().address();
}

bool Human::Connected() const
{
    return socket_.is_open();
}
void Human::Disconnect()
{
    socket_.close();
}
void Human::HandleConnection(const err& ec)
{
    if ( verbose_ ) std::cout << __PRETTY_FUNCTION__ << '=' << (void*)this << '\n';
    if ( ec )
    {
        std::cout << __PRETTY_FUNCTION__ << ec.message() << '\n';
    }
    else
    {
        game_.PlayerAction( shared_from_this(), Action::CONNECT );
        boost::asio::async_write( socket_,
                                 boost::asio::buffer("What is your name?\r\n"),
                                 std::bind(&Human::WaitForName, this, ph::_1) );
        Touch();
    }
}
void Human::WaitForName(const err& ec)
{
    if ( verbose_ ) std::cout << __PRETTY_FUNCTION__ << '=' << (void*)this << ';' << ec.message() << '\n';
    if ( !ec && socket_.is_open() )
    {
        boost::asio::async_read_until( socket_,
                                       buffer_,
                                       &line_ending,
                                       std::bind(&Human::GetName,
                                               this,
                                               ph::_1,
                                               ph::_2 ) );
        Touch();
    }
    else
    {
        std::cout << __PRETTY_FUNCTION__ << ec.message() << std::endl;
        game_.PlayerAction( shared_from_this(), Action::DISCONNECT );
    }
}

void Human::GetName( const err& ec, const size_t& bytes )
{
    if ( verbose_ ) std::cout << __PRETTY_FUNCTION__ << '=' << (void*)this << ';' << ec.message() << ';' << bytes << '\n';
    if ( ec )
    {
        std::cout << __PRETTY_FUNCTION__ << ec.message() << std::endl;
        socket_.close();
    }
    else if ( ! bytes )
    {
        Say( "Please enter a name." );
    }
    else
    {
        name_.clear();
        auto b = boost::asio::buffer_cast<const char*>(buffer_.data());
        auto e = b + std::min(bytes,std::size_t(40));
        std::copy_if( b, e, std::back_inserter(name_),
                      [](char c){return !!std::isalnum(c);} );
        buffer_.consume( bytes );
        if ( name_.empty() || boost::istarts_with(name_,"ai") || boost::istarts_with(name_,"strawman") )
        {
            Say("Not a good name.");
            socket_.close();
        }
        else
        {
            if ( Load() )
            {
                Prompt( "What is the password"+color::INVISIBLE, std::bind(&Human::CheckPassword,this,ph::_1) );
            }
            else
            {
                Login();
            }
        }
    }
    if ( ! socket_.is_open() )
    {
        game_.PlayerAction( shared_from_this(), Action::DISCONNECT );
    }
}
void Human::Login()
{
    game_.PlayerAction( shared_from_this(), Action::NAME );
    std::string msg{ color::NONE+"Welcome, " };
    msg += name_
           + ". Consider typing 'help' as commands have changed.\r\n"
           + "You have colors "
           +(color_?"en":"dis")
           +"abled and UTF-8 "
           +(utf8_?"en":"dis")
           +"abled.\r\n";
    if ( verbose_ )
        std::cout << "I'm so happy to meet '" << name_ << "'.\n";
    boost::asio::async_write( socket_,
                            boost::asio::buffer(msg),
                            std::bind(&Human::WaitForCommand, this, ph::_1) );
    Touch();
}

void Human::WaitForCommand(const err& ec)
{
    if ( verbose_ ) std::cout << __PRETTY_FUNCTION__ << '=' << (void*)this << ';' << ec.message() << '\n';
    if ( buffer_.size() > 99 )
    {
        std::cout << "WARNING: the buffer for " << name_ << " has grown to be "
                  << buffer_.size() << " bytes. Dropping it all! Discarded:"
                  << boost::asio::buffer_cast<const char*>(buffer_.data()) << '\n';
        buffer_.consume( buffer_.size() );
    }
    if ( !ec && socket_.is_open() )
    {
        boost::asio::async_read_until( socket_,
                                       buffer_,
                                       &line_ending,
                                       std::bind(&Human::ParseCommand,
                                               this,
                                               ph::_1,
                                               ph::_2 ) );
        Touch();
    }
    else
    {
        std::cout << __PRETTY_FUNCTION__ << " error_code=" << ec.value()
                  << " (" << ec.message()
                  << ") socket " << (socket_.is_open()?"is":"is not")
                  << " open.\n";
        game_.PlayerAction( shared_from_this(), Action::DISCONNECT );
    }
}

void Human::ParseCommand(const err& ec, const size_t& bytes)
{
    if ( verbose_ ) std::cout << __PRETTY_FUNCTION__ << '=' << (void*)this << ';' << ec.message() << ';' << bytes << '\n';
    if ( ec.value() == 3 )
    {
        Say("Your command is too long.");
        buffer_.consume( buffer_.size() );
        WaitForCommand(err());
        return;
    }
    else if ( ec )
    {
        std::cout << "Error in " << __PRETTY_FUNCTION__ << " ec=" << ec.value() << ": " << ec.message() << '\n';
    }
    else
    {
        auto b = boost::asio::buffer_cast<const char*>(buffer_.data());
        auto e = std::next( b, bytes );
        if ( verbose_ )
        {
            std::cout << "Exact bytes received from " << name_ << ':';
            for ( auto it = b; it != e; ++it )
            {
                WriteChar(*it);
            }
            std::cout.put('|');
            if ( buffer_.size() > bytes )
            {
                std::cout << " Extraneous data in the buffer:";
                for ( auto i = bytes; i < buffer_.size(); ++i )
                {
                    WriteChar( b[i] );
                }
            }
            std::cout.put('\n');
        }
        std::string cmd;
        std::transform( b, e, std::back_inserter(cmd),
                        [](char c){return (iscntrl(c)||isspace(c))?' ':c;} );
        boost::trim( cmd );
        if ( cmd.empty() )
        {
            std::cout << "Did " << name_ << " just hit enter excessively?\n";
        }
        if ( verbose_ )
        {
            std::cout << bytes << " bytes received. Buffer size is " << buffer_.size() << ". Consuming... ";
        }
        if ( bytes <= buffer_.size() )
            buffer_.consume( bytes );
        if ( verbose_ )
        {
            std::cout << "Buffer is now " << buffer_.size() << " bytes\n"
                      << "Command is '" << cmd << "'\n";
        }
        if ( prompt_callback_ )
        {
            prompt_callback_( std::string(b, e) );
            prompt_callback_ = decltype(prompt_callback_)();
            WaitForCommand(ec);
            return;
        }
        if ( verbose_ )
            std::cout << name_ << " said: '" << cmd << "'.\n";
        if ( boost::istarts_with(cmd, "shutdown") && boost::iequals(name_,"john") )
        {
            socket_.get_io_service().stop();
            std::cout << name_ << " has shut the server down.\n";
            return;
        }
        boost::split( command_arguments_, cmd, boost::is_any_of(" ") );
        command_arguments_.erase( std::remove(command_arguments_.begin(), command_arguments_.end(), ""), command_arguments_.end() );
        int i;
        std::ostringstream o;
        if ( ! command_arguments_.empty() && ! command_arguments_[0].empty() )
        {
            std::ostringstream oss;
            switch ( std::tolower(command_arguments_[0].front()) )
            {
                case 'a':
                    argument_ = "AI";
                    game_.PlayerAction( shared_from_this(), Action::JOIN );
                    break;
                case 'c':
                    if ( ! cantrip_ )
                    {
                        Say( "You have no cantrip ready." );
                    }
                    else if ( cantrip_(shared_from_this()) )
                    {
                        cantrip_ = decltype(cantrip_)();
                    }
                    break;
                case 'd':
                    game_.PlayerAction(shared_from_this(), Action::DUCK);
                    break;
                case 'e':
                    Save();
                    game_.PlayerAction(shared_from_this(), Action::DISCONNECT);
                    socket_.close();
                    break;
                case 'f':
                    color_ = !color_;
                    if ( color_ )
                        utf8_ = !utf8_;
                    oss << "You now will see output "
                        << ( color_ ? "with" : "without" )
                        << " ANSI colorizing escape codes and "
                        << ( utf8_ ? "with" : "without" )
                        << " UTF-8 characters outside the Latin-1 code page.";
                    Save();
                    Say( oss.str() );
                    break;
                case 'h':
                    Say( Help(command_arguments_) );
                    break;
                case 'j':
                    argument_ = name_;
                    game_.PlayerAction(shared_from_this(), Action::JOIN);
                    break;
                case 'l':
                    game_.PlayerAction(shared_from_this(), Action::LOAD);
                    break;
                case 'p':
                    if ( command_arguments_.size() < 2 )
                    {
                        Say("You must specify what your new password is.");
                    }
                    else if ( command_arguments_.size() > 2 )
                    {
                        Say("Whitespace and such is not supported in passwords.");
                    }
                    else
                    {
                        auto p = command_arguments_.at(1);
                        password_.clear();
                        std::copy_if( p.begin(), p.end(), std::back_inserter(password_), ::isgraph );
                    }
                    if ( password_.empty() )
                    {
                        Say("Setting of the password failed.");
                    }
                    else
                    {
                        Say(std::string("Your password is now '")+password_+"'.");
                        Save();
                    }
                    break;
                case 'r':
                    if ( calcs_.empty() )
                    {
                        Say("No battle calculations available for you to review." );
                        break;
                    }
                    i = calcs_.size() - 1;
                    if ( command_arguments_.size() > 1 )
                    {
                        i = atoi( command_arguments_.at(1).c_str() );
                        if ( i < 0 )
                        {
                            i = 1 + i;
                        }
                        else
                        {
                            i = calcs_.size() - i - 1;
                        }
                    }
                    if ( i >= static_cast<int>(calcs_.size()) )
                        i = calcs_.size() - 1;
                    if ( i < 0 )
                        i = 0;
                    calcs_.at( i ).Audit( o );
                    Say( o.str() );
                    break;
                case 's':
                    if ( command_arguments_.size() == 1 )
                    {
                        Say("You have to specify whom you wish to shoot in a shoot command.");
                    }
                    else if ( boost::iequals(name_,command_arguments_.at(1)) )//Note this allows you to commit suicide if you have a prefix long enough to be unique but not be your entire name
                    {
                        Say("No suicide, please.");
                    }
                    else
                    {
                        argument_.assign(command_arguments_.at(1));
                        game_.PlayerAction(shared_from_this(), Action::SHOOT);
                    }
                    break;
                case 't':
                    if ( command_arguments_.size() < 3 )
                    {
                        Say("You need to specify to whom you wish to speak, and what you'd like to say.");
                    }
                    else
                    {
                        auto sp = cmd.find(' ');
                        sp = cmd.find(' ', sp+1);
                        argument_.assign( cmd, sp + 1, std::string::npos );
                        game_.PlayerAction( shared_from_this(), Action::TELL );
                    }
                    break;
                case 'w':
                    if ( command_arguments_.size() < 2 )
                    {
                        game_.PlayerAction( shared_from_this(), Action::WHO );
                    }
                    else
                    {
                        auto viewing = game_.ResolvePrefix( command_arguments_.at(1), true );
                        if ( viewing.empty() )
                        {
                            Say( command_arguments_.at(1) + " is not the name of a logged-in player." );
                        }
                        for ( auto v : viewing )
                        {
                            Say( "========================================" );
                            Say( color::WHITE + v->name_ + color::NONE );
                            Say( std::string("Ammo  : ") + std::to_string(v->ammo_) );
                            Say( std::string("Wins  : ") + std::to_string(v->Wins()) );
                            Say( std::string("Losses: ") + std::to_string(v->Losses()) );
                            Say( std::string("Kills : ") + std::to_string(v->Kills()) );
                            Say( std::string("Luck  : ") + std::to_string(v->Luck()) );
                            Say( std::string("Rating: ") + std::to_string(v->rating_) );
                            auto human = dynamic_cast<Human*>( v.get() );
                            if ( human && human->badges_.size() )
                            {
                                Say( "#########################" );
                                Say( "         Badges" );
                                for ( auto b : human->badges_ )
                                {
                                    Say( DisplayText(b) );
                                }
                                Say( "#########################" );
                            }
                        }
                    }
                    break;
                case '\'':
                    if ( cmd.size() == 1 )
                    {
                        Say("What did you want to say?");
                    }
                    else
                    {
                        argument_.assign( cmd.substr(1) );
                        boost::trim( argument_ );
                        game_.PlayerAction(shared_from_this(), Action::SAY );
                    }
                    break;
                default:
                    Say(cmd+" is not a valid command.");
            }
        }
    }
    if ( socket_.is_open() )
    {
        WaitForCommand(ec);
    }
    else
    {
        game_.PlayerAction( shared_from_this(), Action::DISCONNECT );
    }
}
void Human::Say( std::string message )
{
    if ( verbose_ )
        std::cout << "Telling " << name_ << " '" << message << "'.\n";
    if ( !color_ )
    {
        for ( auto esc = message.find('\e'); esc != std::string::npos; esc = message.find('\e') )
        {
            auto m = message.find( 'm', esc );
            if ( m == std::string::npos )
                break;
            message.erase( esc, m - esc + 1 );
            if ( esc >= message.size() )
                break;
        }
        if ( verbose_ )
            std::cout << "'After removing colors the message is: " << message << "'\r\n";
    }
    if ( !utf8_ )
    {
        auto o = message.begin();
        bool extended = false;
        for ( char c : message )
        {
            if ( c < 0 )
            {
                extended = true;
            }
            else if ( extended )
            {
                extended = false;
            }
            else
            {
                *o = c;
                ++o;
            }
        }
        message.erase( o, message.end() );
        if ( verbose_ )
            std::cout << "After removing extended characters the message is: '" << message << "'\r\n";
    }
    message.push_back('\r');
    message.push_back('\n');
    if ( delayed_output_.empty() )
    {
        boost::asio::async_write( socket_,
                                  boost::asio::buffer(message),
                                  std::bind(&Human::Said,this,ph::_1,ph::_2) );
    }
    else
    {
        delayed_output_.append( message );
    }
}

void Human::Said(const boost::system::error_code& ec, const size_t& bytes)
{
    if ( ec || ! socket_.is_open() )
    {
        game_.PlayerAction( shared_from_this(), Action::DISCONNECT );
    }/*
    else if ( verbose_ )
    {
        std::cout << bytes << " sent to " << name_ << '\n';
    }*/
}

void Human::Prompt( const std::string& text, std::function<void(const std::string&)> cb )
{
    Say( text + "?" );
    prompt_callback_ = cb;
    err ec;
    WaitForCommand(ec);
}
void Human::Become( const Player& src )
{
    Player::Become( src );
    name_ = src.name_;
    ammo_ = src.ammo_;
    ducked_ = src.ducked_;
    duck_count_ = src.duck_count_;
    argument_ = src.argument_;
    color_ = src.color_;
    utf8_ = src.utf8_;
    cantrip_ = src.cantrip_;
    command_arguments_ = src.command_arguments_;
    password_ = src.password_;
    verbose_ = src.verbose_;
    Touch();
}
void Human::DeltaLuck(int d)
{
    luck_ += d;
    Save();
    if ( luck_ > 50 )
        EarnBadge( Badge::LUCK_O_THE_LOSERS );
}
void Human::ScoreKill()
{
    Player::ScoreKill();
    Save();
}
void Human::Win()
{
    Player::Win();
    if ( wins_ > 100 )
        EarnBadge( Badge::THE_VETERAN );
    Save();
}
void Human::Lose()
{
    Player::Lose();
    Save();
}
std::map< std::string, std::function< void(Human*,const std::string&)> > Human::ReadDatum()
{
    static std::map<std::string,std::function<void(Human*,const std::string&)> > rv;
    if ( rv.empty() )
    {
        rv["wins"] = [](Human*h,const std::string&v){h->Wins(atoi(v.c_str()));};
        rv["losses"] = [](Human*h,const std::string&v){h->Losses(atoi(v.c_str()));};
        rv["kills"] = [](Human*h,const std::string&v){h->Kills(atoi(v.c_str()));};
        rv["luck"] = [](Human*h,const std::string&v){h->Luck(atoi(v.c_str()));};
        rv["color"] = [](Human*h,const std::string&v){h->color_=v!="false";};
        rv["utf8"] = [](Human*h,const std::string&v){h->utf8_=v!="false";};
        rv["password"] = [](Human*h,const std::string&v){h->password_.clear();std::copy_if(v.begin(),v.end(),std::back_inserter(h->password_),::isgraph);};
        rv["rating"] = [](Human*h,const std::string&v){h->rating_=atoi(v.c_str());};
        rv["badge"] = [](Human*h,const std::string&v){Badge b;std::istringstream i(v);i >> b;h->badges_.insert(b);};
    }
    return rv;
}
void Human::Save()
{
    if ( password_.empty() )
        return;
    std::ofstream f( (DATA_DIR / boost::algorithm::to_lower_copy(name_)).string() );
    f << "3\n"//file format version 3
      << "wins:" << Wins() << '\n'
      << "losses:" << Losses() << '\n'
      << "kills:" << Kills() << '\n'
      << "luck:" << Luck() << '\n'
      << "color:" << std::boolalpha << color_ << '\n'
      << "utf8:" << std::boolalpha << utf8_ << '\n'
      << "password:" << password_ << '\n'
      << "rating:" << rating_ << '\n';
    for ( auto b : badges_ )
    {
        f << "badge:" << b << '\n';
    }
    f.put( '\n' );
}
bool Human::Load()
{
    auto p = DATA_DIR / boost::algorithm::to_lower_copy(name_);
    if ( !boost::filesystem::exists(p) )
    {
        std::cout << p << " does not exist.\n";
        return false;
    }
    std::ifstream f( p.string() );
    int version = -1;
    f >> version;
    if ( version == 3 )
    {
        std::string l;
        try
        {
            while ( f.good() )
            {
                l.clear();
                std::getline(f, l);
                auto colon = l.find(':');
                if ( colon != std::string::npos && colon > 0 && colon < l.size() )
                {
                    ReadDatum().at(l.substr(0,colon))( this, l.substr(colon+1) );
                }
            }
        }
        catch  ( const std::out_of_range& oor )
        {
            std::cout << "ERROR reading in " << p << ": the line '" << l << "' does not contain a valid key. "
                      << oor.what() << std::endl;
        }
        return true;
    }
    if ( version < 0 || version > 2 )
    {
        std::cout << "File version " << version << " not supported.\n";
        return false;
    }
    int i = 0;
    f >> i;
    Wins(i);
    f >> i;
    Losses(i);
    f >> i;
    Kills(i);
    f >> i;
    Luck(i);
    if ( version == 2 )
    {
        f >> std::boolalpha >> color_;
        f >> std::boolalpha >> utf8_;
    }
    password_.clear();
    //while ( f.good() && f.get() != '\n' ) ;
    while ( password_.empty() && f.good() )
        std::getline( f, password_ );
    if ( password_.empty() )
    {
        std::cout << "No password was in the file " << p << '\n';
        return false;
    }
    if ( !version )
        return true;
    f >> i;
    Badge b = Badge::NO_BADGE;
    for ( ; i; --i )
    {
        f >> b;
        badges_.insert( b );
    }
    return true;
}
void Human::CheckPassword( std::string entered_pw )
{
//     auto i = password_.begin();
//     for ( auto e : entered_pw )
//     {
//         if ( isgraph(e) )
//         {
//             for ( ; i != password_.end() && !isgraph(*i); ++i ) ;
//             if ( i == password_.end() || *i != e )
//             {
//                 Say("Wrong password.");
//                 socket_.get_io_service().post( std::bind(&Human::Disconnect,this) );
//                 Disconnect();
//                 return;
//             }
//             ++i;
//         }
//     }
    entered_pw.erase( std::remove_if(entered_pw.begin(), entered_pw.end(),[](char c){return!::isgraph(c);}), entered_pw.end() );
    if ( boost::iequals(entered_pw,password_) )
    {
        Login();
    }
    else
    {
        Say( color::NONE+"Wrong." );
        game_.PlayerAction(shared_from_this(), Action::DISCONNECT);
        socket_.close();
    }
}

void Human::EarnBadge(Badge b)
{
    if ( badges_.count(b) )
    {
        std::cout << name_ << " re-earned " << DisplayText(b) << '\n';
    }
    else
    {
        badges_.insert(b);
        Say( color::WHITE + "You have earned a badge!\r\n" + DisplayText(b) );
        if ( badges_.size() == BadgeCount() - 1 )
        {
            EarnBadge( Badge::EMIL_BLEEHALL );
        }
        else if ( password_.empty() )
        {
            Say( "This would probably be a good time to save your stats (and badges!) by typing password <yourpassword>" );
        }
        else
        {
            Save();
        }
    }
}

void Human::RoundBegins()
{
    Player::RoundBegins();
}
void Human::DisplayAnimation(ani::Mation& v)
{
    if ( delayed_output_.empty() )
        delayed_output_.push_back(' ');
    frame_b_ = v.begin();
    frame_e_ = v.end();
    game_.Post( std::bind(&Human::ShowFrame, this) );
}

void Human::ShowFrame()
{
    if ( frame_b_ == frame_e_ )
    {
        std::string s = "\r\n";
        s.append( delayed_output_ );
        delayed_output_.clear();
        Say( s );
    }
    else
    {
        boost::asio::async_write( socket_,
                                  boost::asio::buffer(frame_b_->first),
                                  std::bind(&Human::Said,this,ph::_1,ph::_2) );
        ++frame_b_;
        animation_timer_.expires_from_now( frame_b_->second );
        animation_timer_.async_wait( std::bind(&Human::ShowFrame, this) );
    }
}
