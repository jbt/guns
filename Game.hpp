/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2014  John B. Turpish <john.turpish@tolling.telvent.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef SERVER_H
#define SERVER_H

#include "Move.hpp"
#include <boost/asio/ip/tcp.hpp>
#include <boost/range/join.hpp>
#include <boost/filesystem/path.hpp>
#include <list>
#include <vector>

class Game
{
public:
    Game( boost::asio::io_service& service, uint16_t port, bool verbose );
    void PlayerAction(std::shared_ptr<Player> who, Player::Action action);
    std::vector<std::shared_ptr<Player> > ResolvePrefix(const std::string& prefix, bool include_specators=false) const;
    std::vector<std::shared_ptr<Player> > ResolvePrefix(const std::string& prefix, bool include_specators=false);
    void Post( std::function<void()> foo );
    int round_size_;

private:
    enum PlayerState
    {
        DISCONNECTED,
        CONNECTED,
        JOINED,
        THINKING,
        ACTED,
        STATE_COUNT
    };
    boost::asio::ip::tcp::acceptor acceptor_;
    std::vector<std::list<std::shared_ptr<Player> > > players_;
    bool verbose_;
    short timer_ticks_;
    short round_count_;
    bool ready_to_start_;
    bool warned_;
    boost::asio::deadline_timer timer_;
    std::vector<Move> moves_;
    int delay_;
    std::random_device r_;
    std::shared_ptr<Player> mAi;

    void Listen();
    bool StateChange(Player& who, PlayerState old_state, PlayerState new_state);
    bool StateChange(const std::shared_ptr<Player>& who, PlayerState old_state, PlayerState new_state);
    void Broadcast( const std::string& message );
    void ResetTimer();
    void ProcessRound();
    void StartTimer();
    void TimerFired(short ticks, const boost::system::error_code& ec);
    void ScoreBoard();
    auto NotPlaying() const -> decltype(boost::join(players_[THINKING], players_[ACTED]))
    {
        return boost::join(players_[CONNECTED], players_[JOINED]);
    }
    auto NotPlaying() -> decltype(boost::join(players_[THINKING], players_[ACTED]))
    {
        return boost::join(players_[CONNECTED], players_[JOINED]);
    }
    void AddCantrips();
    struct Cantrip
    {
        const char* title_;
        const char* description_;
        Player::Cantrip foo_;
    };
    void AddCantrip( Player& p, Cantrip& c ) const;
    std::vector<Cantrip> cantrips_;
    std::vector<Cantrip> BuildCantrips();
    void CheckPassword( std::shared_ptr<Player> candidate, std::shared_ptr<Player> against, const std::string& response );
    void CapLuck();
    std::vector<std::shared_ptr<Player> > AllPlayers();
public:
    std::size_t cantrip_debug_;
    auto Playing() const -> decltype(boost::join(players_[THINKING], players_[ACTED]))
    {
        return boost::join(players_[THINKING], players_[ACTED]);
    }
    auto Playing() -> decltype(boost::join(players_[THINKING], players_[ACTED]))
    {
        return boost::join(players_[THINKING], players_[ACTED]);
    }
};

extern boost::filesystem::path DATA_DIR;

#endif // SERVER_H
