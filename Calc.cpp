#include "Calc.hpp"
#include "Player.hpp"
#include <iomanip>

bool Calc::Evaluate( const std::map<std::string,int>& order, int round, ExtraResults& res, const Player& shooter, const Player& target, std::random_device& r )
{
    round_ = round;
    shooter_luck_ = shooter.Luck();
    target_luck_ = target.Luck();
    shooter_name_ = shooter.name_;
    target_name_ = target.name_;
    shooter_first_ = order.at(shooter.name_) < order.at(target.name_);
    target_ducked_ = target.ducked_;
    duck_count_ = target.duck_count_;
    int hits, misses;
    hits = misses = 0;
    BuildProbabilities( hits, misses, std::cout );
    std::uniform_int_distribution<int> d( 0, hits + misses );
    random_number_ = d(r);
    return Resolve( hits, misses, res, std::cout );
}

void SumLine( std::ostream& o, int hits, int misses, const std::string& why )
{
    o << std::setw(10) << hits << " | ";
    o << std::setw(11) << misses << " | " << why << "\r\n";
}
void Calc::BuildProbabilities(int& hits, int& misses, std::ostream& o) const
{
    o << shooter_name_ << " shoots " << target_name_ << " in round " << round_ << "\r\n"
      << "HitChances | MissChances | Why \r\n";
    if ( target_ducked_ )
    {
        hits = 0;
        misses = 24;
        SumLine( o, hits, misses, target_name_ + " ducked." );
        hits = duck_count_;
        SumLine( o, hits, misses, std::string("The number of times ")+target_name_ + " has ducked in this game." );
    }
    else
    {
        hits = 9;
        misses = 0;
        SumLine( o, hits, misses, target_name_ + " had not ducked." );
    }
    if ( shooter_first_ )
    {
        ++hits;
        SumLine( o, hits, misses, shooter_name_ + " entered a move first." );
    }
    else
    {
        ++misses;
        SumLine( o, hits, misses, target_name_ + " entered a move first." );
    }
    hits += shooter_luck_;
    SumLine( o, hits, misses, shooter_name_ + "'s luck modifier." );
    misses += target_luck_;
    SumLine( o, hits, misses, target_name_ + "'s luck modifier." );
    auto shift = std::min( hits, misses );
    if ( shift < 1 )
    {
        hits += -shift + 1;
        misses += -shift + 1;
        SumLine( o, hits, misses, "Domain shift (both outcomes must be possible)." );
    }
}
bool Calc::Resolve(int hits, int misses, ExtraResults& res, std::ostream& o ) const
{
    res.percent_ = ( hits * 1000 / (hits+misses) ) / 10.0;
    o << hits << "/(" << hits << '+' << misses << ")=" << res.percent_ << "%\r\n"
      << "A random number between 0 and " << ( hits + misses ) << " was chosen and it was "
      << random_number_ << ".\r\n";
    auto hit = random_number_ < hits;
    o << random_number_ << ( hit ? " is" : " is not" ) << " less than " << hits << " and therefore the shot "
      << ( hit ? "hits " : "misses " ) << target_name_ << "\r\n";
    if ( hit )
    {
        res.shooter_luck_mod_ = 0;
        if ( misses > hits )
        {
            o << "This was an unlikely event, and so " << target_name_ << " gains a point of luck.\r\n";
            res.target_luck_mod_ = 1;
        }
        else
        {
            res.target_luck_mod_ = 0;
        }
        if ( target_ducked_ )
        {
            o << target_name_ << " was ducked when shot, and that feels wrong. A point of luck.\r\n";
            res.target_luck_mod_++;
        }
        if ( misses > hits && target_ducked_ )
        {
            o << "Because of the combination of being lucky and hitting while ducked, " << shooter_name_
              << " loses a point of luck.\r\n";
            res.shooter_luck_mod_ = -1;
        }
        else
        {
            res.shooter_luck_mod_ = 0;
        }
        o << "A 'better luck next time' point of luck modifier for " << target_name_ << ".\r\n";
    }
    else
    {
        if ( hits > misses )
        {
            o << "This was an unlikely scenario, so a point of luck for " << shooter_name_ << "\r\n";
            res.shooter_luck_mod_ = 1;
        }
        else
        {
            res.shooter_luck_mod_ = 0;
        }
        if ( !target_ducked_ )
        {
            o << target_name_ << " wasn't even ducked. " << shooter_name_ << " needs to work on his aim, so "
              << "here's a point of luck.\r\n";
            res.shooter_luck_mod_++;
        }
        if ( hits > misses && !target_ducked_ )
        {
            o << target_name_ << " due to the combination of not being ducked and having this go unexpectedly in his/her favor loses a point of luck.\r\n";
            res.target_luck_mod_ = -1;
        }
        else
        {
            res.target_luck_mod_ = 0;
        }
    }
    return hit;
}
bool Calc::Audit(std::ostream& log) const
{
    int hits, misses;
    BuildProbabilities( hits, misses, log );
    ExtraResults result;
    return Resolve( hits, misses, result, log );
}
