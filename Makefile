default: bin/guns
	echo $^ built.

prepare:= $(shell mkdir .objs/ bin 2>&- ) 
srcs:= $(wildcard *.cpp)
objs:= $(patsubst %.cpp, .objs/%.o , $(srcs) )
incdirs:= $(addprefix -I , $(HOME)/.local/include ) 
libpath:= -L $(HOME)/.local/lib
libs:= $(addprefix -l , $(addprefix boost_, regex thread filesystem system ) rt ) -pthread
headers:=$(wildcard *.hpp)
warns:= $(addprefix -W, all extra error no-unused-parameter )

debug?= -g -ggdb -O0
ifeq ($(strip $(findstring true,$(static))),true)
STATIC:= -static -static-libstdc++
else
STATIC:= $(static)
endif

bin/guns: $(objs)
	$(CXX) -o $@ -std=c++11 $(debug) $(STATIC) $^ $(libpath) $(libs)

.objs/%.o : %.cpp $(headers)
	- mkdir $(dir $@) 2>/dev/null
	$(CXX) -c -o $@ $< -std=c++11 $(debug) $(STATIC) $(warns) $(incdirs) 

clean :
	- find bin .objs -type f -exec rm '{}' +

