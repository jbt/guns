#include "Ai.hpp"
#include "Move.hpp"
#include "Game.hpp"
#include "Human.hpp"
#include <boost/algorithm/string.hpp>

Ai::Ai( bool verbose, Game* game )
: Npc( *game, verbose ),
    game_( game ),
    danger_(0), opponent_count_(0), grudges_(),
    d_(0,30)
{
    name_ = "AI";
}

void Ai::NotifyOfMove(const Move& m)
{
    if ( IsStrawman(m.who_->name_) || m.who_.get() == this )
    {
        if ( verbose_ )
            std::cout << name_ << " is ignoring the moves from " << m.who_->name_ << '\n';
        return;
    }
    else if ( verbose_ )
    {
        std::cout << "Considering recent action and state of " << m.who_->name_
                  << " in evaluating the atmosphere of the game.\n";
    }
    ++opponent_count_;
    danger_ += m.who_->ammo_;
    /* My theory is that players prefer not to make the same move twice in a row, with the one exception
     * that a few special people will load twice in a row (notably Ricardo and to a lesser degree Daniel,
     * though I've seen Dale pull this stunt a couple times too).
     * By this theory if someone ducks you should expect their next move to be either load or shoot. If
     * the don't have any bullets left, then clearly load. If they do, however, the natural swing-back
     * from ducking is shooting - from defense to offense.
     * Similarly, if someone shoots they're more likely to duck in the next round than in an average
     * round, which may be exacerbated by a fear of retribution.
     */
    if ( m.what_ == Player::Action::SHOOT )
    {
        --danger_;//Because they're about to lose a bullet
        if ( m.who_->ammo_ > 1 )
        {
            --danger_;//by my theory
        }
        else
        {
            targets_[m.who_->name_] += 4;
        }
        if ( std::tolower(m.who_->argument_.at(0)) == 'a' )//even if they're not targeting me, they're thinking about it
        {
            ++danger_;
            grudges_[m.who_->name_]++;
        }
    }
    else if ( m.what_ == Player::Action::DUCK && m.who_->ammo_ > 0 )
    {
        ++danger_;//as discussed
        targets_[m.who_->name_] += 2;
    }
    else if ( m.what_ == Player::Action::LOAD )
    {
        ++danger_;//because they're about to gain a bullet
    }
}

void Ai::RoundBegins()
{
    std::cout << __PRETTY_FUNCTION__ << ':' << danger_ << ';' << opponent_count_ << ';' << duck_count_ << '\n';
    switch ( d_(r_) )
    {
        case 0:
        case 1:
        case 2:
            if ( verbose_ )
                std::cout << "Randomly selected to shoot, ammo=" << ammo_ << '\n';
            if ( ammo_ > 0 )
            {
                argument_.assign( Target() );
                game_->PlayerAction( shared_from_this(), Action::SHOOT );
                break;
            }
        case 3:
        case 4:
            if ( verbose_ )
                std::cout << "Randomly selected to load \n";
            game_->PlayerAction( shared_from_this(), Action::LOAD );
            break;
        case 5:
            if ( verbose_ )
                std::cout << "Randomly selected to duck. \n";
            game_->PlayerAction( shared_from_this(), Action::DUCK );
            break;
        default:
            if ( verbose_ )
                std::cout << "Randomly selected to think. \n";
            //The threshold of acceptable danger increases with duck_count_, because ducking is becoming less effective.
            if ( danger_ >= opponent_count_ + duck_count_ / 2 )
            {
                if ( verbose_ )
                    std::cout << "Game seems dangerous (" << danger_ << '<' << opponent_count_ << '+' << duck_count_ << "). Ducking.\n";
                game_->PlayerAction( shared_from_this(), Action::DUCK );
            }
            else if ( ammo_ < 1 )
            {
                if ( verbose_ )
                    std::cout << "Lock and load.\n";
                game_->PlayerAction( shared_from_this(), Action::LOAD );
            }
            else
            {
                if ( verbose_ )
                    std::cout << "It's time to shoot someone'.\n";
                argument_.assign( Target() );
                game_->PlayerAction( shared_from_this(), Action::SHOOT );
            }
    }
    danger_ = 0;
    opponent_count_ = 0;
    targets_.clear();
}

void Ai::Forgive( Player& p, int based_upon )
{
    auto&& g = grudges_[p.name_];
    if ( g < 1 )
    {
        std::cout << "Looks like " << name_ << " is the aggressor, not "
                  << p.name_ << ".\n";
    }
    else if ( based_upon % 3 )
    {
        if ( verbose_ )
            std::cout << name_ << " forgiving " << p.name_ << " a bit.\n";
        if ( --g == 0 )
        {
            p.Say(name_+" tells you, \"Now we're even.\"");
        }
    }
    else if ( verbose_ )
    {
        std::cout << "Not forgiving " << p.name_ << '\n';
    }
}

namespace {
    template<class ptr>
    ptr SelectByLuck( const std::vector<ptr>& ps, short& r )
    {
        //Looking for the unlucky, who are theoretically better atargets as they're
            //more likely to get hit.
        int maxluck = 1;
        for ( auto&& p : ps )
        {
            if ( p->Luck() >= maxluck )
                maxluck = p->Luck() + 1;
        }
        for ( int i = 0; i < 9; ++i )
        {
            for ( auto&& p : ps )
            {
                if ( typeid(*p) != typeid(Human) )
                    continue;
                r += p->Luck();
                if ( r < maxluck )
                    return p;
                r -= maxluck;
            }
            --r;
        }
        for ( int i = 0; i < 9; ++i )
        {
            for ( auto&& p : ps )
            {
                r += p->Luck();
                if ( r < maxluck )
                    return p;
                r -= maxluck;
            }
            --r;
        }
        return ps.front();
    }
}

std::string Ai::Target()
{
    auto temp = game_->Playing();
    std::vector<std::shared_ptr<Player>> ps;
    std::copy_if( temp.begin(), temp.end(), std::back_inserter(ps),
                  [this](const std::shared_ptr<Player>&p){return p->name_!=name_;} );
    std::vector<long> weights;
    std::transform( ps.begin(), ps.end(), std::back_inserter(weights),
                    [this](std::shared_ptr<Player>&p)
                    {
                        return 3L * grudges_[p->name_] + targets_[p->name_] - p->Luck() / 2;
                    } );
    auto min = *std::min_element(weights.begin(), weights.end());
    if ( min < 1 )
    {
        std::transform(weights.begin(),weights.end(),weights.begin(),[min](long w){return w+1-min;} );
    }
    long total = std::accumulate(weights.begin(),weights.end(),0L);
    std::uniform_int_distribution<long> d( 0, total );
    short selection = d(r_);
    for ( std::size_t i = 0; i <  ps.size() && i < weights.size(); ++i )
    {
        if ( selection < weights[i] )
        {
            return ps[i]->name_;
        }
    }
    return ps.front()->name_;
}
void Ai::GameBegins()
{
    Player::GameBegins();
    targets_.clear();
    danger_ = 0;
    opponent_count_ = 0;
    for ( auto p : game_->Playing() )
    {
        Forgive( *p, d_(r_) );
    }
}
