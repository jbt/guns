#ifndef CALC_HPP_INCLUDED
#define CALC_HPP_INCLUDED 1

#include <map>
#include <string>
#include <random>

class Player;

class Calc
{
public:
    struct ExtraResults
    {
        float percent_;
        int shooter_luck_mod_, target_luck_mod_;
    };
    bool Evaluate( const std::map<std::string,int>& order, int round, ExtraResults& res, const Player& shooter, const Player& target, std::random_device& r );
    bool Audit( std::ostream& log ) const;
private:
    int round_, shooter_luck_, target_luck_, random_number_, duck_count_;
    bool shooter_first_, target_ducked_;
    std::string shooter_name_, target_name_;

    void BuildProbabilities(int& hits, int& misses, std::ostream& o) const;
    bool Resolve( int hits, int misses, ExtraResults& res, std::ostream& o ) const;
};

#endif
