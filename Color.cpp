#include "Color.hpp"

namespace color
{
    extern const std::string GRAY = "\e[00;37m";
    extern const std::string WHITE = "\e[01;37m";
    extern const std::string RED = "\e[01;31m";
    extern const std::string YELLOW = "\e[01;33m";
    extern const std::string BLUE = "\e[01;36m";
    extern const std::string NONE = "\e[01;0m";
    extern const std::string INVISIBLE = "\e[08;30m";
}
