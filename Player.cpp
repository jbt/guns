#include "Player.hpp"
#include "Game.hpp"
#include <boost/algorithm/string.hpp>
#include <boost/date_time.hpp>

namespace ph = std::placeholders;
namespace pt = boost::posix_time;

Player::Player( Game& game, bool verbose )
: game_(game),
  name_("UnknownPlayer"), ammo_(1), ducked_(false),
  duck_count_(0), argument_(), color_(true),
  utf8_(true),
  verbose_( verbose ), rating_(100.0),
  last_active_(pt::second_clock::universal_time()),
  wins_(0), losses_(0), kills_(0), luck_(0)
{
}

Player::~Player()
{
    std::clog << __func__ << '\n';
}


bool Player::operator==(const Player& rhs) const
{
    return this == &rhs;
}

bool Player::Idle() const
{
    return pt::second_clock::universal_time() - last_active_ > pt::minutes(5);
}
bool Player::Inactive() const
{
    if ( verbose_ )
    {
        std::cout << __func__ << '=' << pt::to_iso_string(pt::second_clock::universal_time() - last_active_) << '\n';
    }
    return pt::second_clock::universal_time() - last_active_ > pt::minutes(9);
}
void Player::Touch()
{
    last_active_ = pt::second_clock::universal_time();
}
bool Player::MoreActive(const Player& p) const
{
    return last_active_ < p.last_active_;
}
float Player::WL() const
{
    if ( wins_ || losses_ )
    {
        return ( wins_ * 1000 / (wins_ + losses_) ) / 10.f;
    }
    return 0.f;
}

const char* Player::Help(const std::vector< std::string >& args) const
{
    if ( args.size() < 2 )
    {
        return "Available commands are:\r\n"
            " ai - sign AI up for the next round\r\n"
            " cantrip - use your cantrip if you have one\r\n"
            " duck - attempt evasive manuevers\r\n"
            " exit - disconnect\r\n"
            " font - toggle font characteristics\r\n"
            " join - sign up for the next round\r\n"
            " help - Read this kind of stuff.\r\n"
            " load - add one round of ammunition to your gun\r\n"
            " password - set a password to enable signing back in & save your record\r\n"
            " review - audit recent combat math that involved you \r\n"
            " shoot <target> - attempt to kill [target]\r\n"
            " tell <target> [message] - whisper text into a particular player's ear\r\n"
            " who [target] - See who is currently logged in and what they're doing.\r\n"
            " ' <message> - Speak the text that follows the ' to everyone logged in.\r\n"
            "\r\n"
            "You may use any of those commands as an argument to help, for example you could type help duck. Other help topics include luck and bugs.";
    }
    else
    {
        switch ( std::tolower(args.at(1).at(0)) )
        {
            case 'a':
                return " Anyone can use the ai command, but the consequence is as if AI had run the join "
                       "command. Keep in mind that there is only one AI, so using this command when "
                       "someone has already used it for this upcoming game has no effect.\r\n"
                       " AI is a very simplistic 'artificial intelligence' player - a computer-driven "
                       "player who behaves much like a human would. Its moves are based somewhat on "
                       "randomness, but the choices are heavily weighted toward something that could "
                       "pass as logic. \r\n When it does shoot, its target is chosen largely based on "
                       "vengeance. Which brings up a good tip - if AI ever tells you \"Now we're even\" "
                       "you should strongly consider ducking.";
            case 'b':
                return "This game has plenty of bugs. \r\n"
                       " If you'd like to point some out to me, you may do so, preferably on Diaspora* "
                       "(jbt@diasp.org). But please keep in mind that I'm not as emotionally invested in "
                       "this goofiness as one might imagine, so I might just outright ignore you. \r\n"
                       " If you would like to fix my bugs feel free, but be warned: it isn't pretty. "
                       "https://gitorious.org/jbt/guns/";
            case 'c':
                return "Use a cantrip if you have one.\r\n "
                       "The exact synax will differ with the cantrip. Pay attention to the message that you see "
                       "when you first receive the cantrip. Different cantrips can be used at different times. "
                       "There are even some you can use while a game isn't going on. \r\n "
                       "What is a cantrip? It's a \"little trick\". They are dealt out at the beginning of "
                       "rounds other than the first, to a random selection of players that do not already have "
                       "one. This random group usually has zero players in it, as cantrips are fairly rare. A "
                       "high luck modifier will make it more likely that you get one, though. Which cantrip you "
                       "get is also random, though they are not weighted equally (some are more rare than "
                       "others) and your luck modifier has no impact on which one you get. What they do differs "
                       "significantly, but none of them have a particularly "
                       "significant impact on the game. They do not use your move for the round unless otherwise "
                       "stated.";
            case 'd':
                return "duck\r\n"
                       " If you duck that is your move for the round.\r\n"
                       " It makes those shooting at you less likely to hit, but each subsequent duck during "
                       "a game is less effective as you tire.";
            case 'e':
                return "exit\r\n Disconnect from the server.\r\n If you disconnect by this or any other "
                       "means data about you such as your win/loss ratio, kill count, and luck modifier "
                       "will be lost.";
            case 'f':
                return "font\r\n Toggle characteristics of your font.\r\n Specifically repeatedly using "
                       "the font command will cycle through all possible combinations of enabling ANSI "
                       "color escape codes (vs. monochrome) and enabling non-Latin-1 UTF-8 characters ("
                       "which won't work if you set your character set to something archaic... if you set "
                       "to other Unicode encodings nothing will work.)";
            case 'h':
                return "help\r\nhelp <topic>\r\n Honestly if you don't understand how to use the help "
                       "command you should ask someone to help you out, because you're going to be having "
                       "difficulties.";
            case 'j':
                return "join\r\n"
                       " When you first sign on or after you finish a game you are a spectator.\r\n"
                       " If you issue the join command you will be included in the next game that begins.";
            case 'p':
                return "password <password>\r\n"
                       " If you lose your connection, depending on how and when that happens, it might not "
                       "be detected. In such a case your character is left 'linkdead', out of your "
                       "control. Worse, if you were in a game at the time your character may now be holding "
                       "everyone up as it times out every single round.\r\n"
                       " Normally, if you sign back in with the same name if you were a spectator your "
                       "old connection will be booted and you will start afresh, but if you were in a game "
                       "your new session will get renamed to something ridiculous.\r\n"
                       " If you had set a password you will be prompted for it. If you give the correct "
                       "answer you will pick up right where you left off, with the same luck modifier, win/"
                       "loss record, and so on. You can even participate in a game you were already in (if "
                       "you haven't been killed in the interim.\r\n"
                       " Also, setting a password will save your win/loss/kill record and your luck "
                       "modifier for next time, even if your character gets booted from timeout.\r\n"
                       " It is strongly recommended that you "
                       "not use characters outside the Latin-1 character set in your password. It will "
                       "not behave as you desire.";
            case 'r':
                return "review \r\n review X \r\n "
                       " If X is omitted or zero you will see some information about the last "
                       "shot that involved you (either way). If X is 1 you will see the shot "
                       "before that, 2 the one before that, and so on. If X is -1 you will see "
                       "the oldest shot either taken by you or aimed at you on recent record. "
                       "If X is -2 you will see the one after that, -3 the one after that, and "
                       "so on. \r\n This command has no impact on the game, it is just to "
                       "satiate curiosity about why the probabilites worked out the way they "
                       "did, or where the luck modifiers come from, etc..";
            case 's':
                return "shoot <target>\r\n Shooting at another player is your move for the round.\r\n"\
                       " If you successfully hit them they will be out of the game and you'll be one step "
                       "closer to winning.\r\n"
                       " Regardless of whether you hit them, you'll have one fewer round of ammo. If you "
                       "have zero you'll be unable to shoot until you load.";
            case 't':
                return "tell <target> <message> \r\n Tell the target (a player) the given message. \r\n "
                       "Closely related to say, this message will only be received by one player.";
            case 'w':
                return "who\r\n List out all the players currently logged in, grouped by what "
                       "their current status is (made their move, thinking, waiting for the next game, spectating)"
                       ".\r\nwho <name> \r\n get some info about the named player. If <name> is a prefix that "
                       "does not uniquely specify a single player, get statistics etc. about each of those players.";
            case '\'':
                return "' <text>\r\n This command will send <text> to every logged-in player including "
                       "yourself. There are two things worth watching out for:\r\n Whitespace will be "
                       "trimmed off the beginning and end of your message.\r\n Sending more than sixty-four "
                       "bytes (which commonly means 64 characters) is dangerous, probably won't do what you "
                       "want and is not recommended.";
            case 'l':
                if ( args.at(1).size() < 2 )
                    return "Did you mean load or luck?";
                if ( args.at(1).at(1) == 'o' )
                {
                    return "load \r\n"
                        " If you load your weapon that consumes your move for the round.\r\n"
                        " Loading increases your ammo count by one, making future shoot commands possible.\r\n"
                        " Ammo does not carry over from one game to the next.";
                }
                else if ( args.at(1).at(1) == 'u' )
                {
                    return "We all know that if you play a game that's heavily influenced by random chance "
                           "(as this one is) for an infinite amount of time the effect of chance averages "
                           "out. But what if you don't have an infinite amount of time?\r\n"
                           "The luck modifier is one of the factors that influences a person's chance of "
                           "getting shot or successfully shooting someone else (vs. missing). There are a "
                           "number of situations that are deemed to feel 'unlucky' and cause your luck "
                           "modifier to increase, and a few 'lucky' situations that can cause it "
                           "to decrease. You can view all current luck modifiers using the who command.\r\n"
                           "When a person disconnects by any means their modifier is reset to zero.\r\n "
                           " Your luck modifier increases when: \r\n"
                           "  You shoot at someone who was not ducked, but missed.\r\n"
                           "  You shoot at someone and had over 50% chance of hittin them, but missed.\r\n"
                           "  Someone shoots you while you're ducked'.\r\n"
                           "  Someone shoots at you with less than 50% chance of hitting, but hits you anyhow.\r\n"
                           "  You get shot.\r\n"
                           " Note that these are cumulative, so if you duck and someone successfully shoots you with a 20% chance you get +3 on your luck.\r\n"
                           " Your luck decreases when:\r\n"
                           "  You shoot someone who was ducked when you had less than 50% chance of hitting them.\r\n"
                           "  Someone shoots at you and misses when you were not ducked and they had greater than 50% chance of hitting.\r\n"
                           "  At the end of a round:\r\n"
                           "   If you have the highest luck modifier (tie broken arbitrarily) and you have the best win record (tie broken the same way), your luck decreases by one.\r\n"
                           "   If all humans have a positive luck modifier, each player with a positive modifier loses one point of luck.\r\n"
                           "   If either of these happen to you, you will be notified that your luck is running out.\r\n";
                }
            default:
                return "Unknown help topic.\r\n";
        }
    }
}
void Player::RoundBegins()
{
}
void Player::Prompt( const std::string& text, std::function<void(const std::string&)> cb )
{
    throw std::runtime_error( text + " was asked... "+__PRETTY_FUNCTION__ );
}
void Player::Become( const Player& src )
{
    wins_ = src.wins_;
    losses_ = src.losses_;
    kills_ = src.kills_;
    luck_ = src.luck_;
}
void Player::NotifyOfMove(const Move& m)
{

}
int Player::Luck() const
{
    return luck_;
}
namespace {
    void ScaledRating( double& jump_rating )
    {
        jump_rating *= 1249;
        jump_rating -= 250;
    }
}
void Player::Lose()
{
    if ( ++losses_ > 1000 )
    {
        EarnBadge( Badge::THE_BRAVE_AND_THE_PERSISTENT );
    }
    double round_rating = 0.5 * ( game_.round_size_ - 1.0 ) / game_.round_size_;
    ScaledRating( round_rating );
    int jump_rating = ( 9.0 * rating_ + round_rating ) / 10.0;
    if ( verbose_ )
        std::cout << __func__ << ',' << name_ << ": round_rating is " << round_rating
                  << ", rating_ is " << std::dec << rating_ << ", jump rating is "
                  <<  jump_rating << '\n';
    if ( jump_rating > -1 && jump_rating < rating_ )
    {
        if ( verbose_ )
            std::cout << name_ << " Rating jumps down from " << rating_ << " to " << jump_rating << '\n';
        rating_ = jump_rating;
    }
    else
    {
        --rating_;
        if ( verbose_ )
            std::cout << name_ << " Rating steps down to " << rating_ << '\n';
    }
    CapRating();
}
void Player::ScoreKill()
{
    ++kills_;
    ++rating_;
    CapRating();
}
void Player::Win()
{
    ++wins_;
    double round_rating = 1.0 - 0.5 / game_.round_size_;
    ScaledRating( round_rating );
    int jump_rating = ( 9.0 * rating_ + round_rating ) / 10.0;
    if ( verbose_ )
        std::cout << __func__ << ',' << name_ << ": round_rating is " << round_rating
                  << ", rating_ is " << std::dec << rating_ << ", jump rating is "
                  <<  jump_rating << '\n';
    if ( jump_rating < 1000 && jump_rating > rating_ )
    {
        if ( verbose_ )
            std::cout << name_ << " Rating jumps up from " << rating_ << " to " << jump_rating << '\n';
        rating_ = jump_rating;
    }
    else
    {
        rating_ += 3;
        if ( verbose_ )
            std::cout << name_ << " Rating steps up to " << rating_ << '\n';
    }
    CapRating();
}
int Player::Kills() const
{
    return kills_;
}
int Player::Losses() const
{
    return losses_;
}
int Player::Wins() const
{
    return wins_;
}
void Player::Kills(int val)
{
    kills_ = val;
}
void Player::Losses(int val)
{
losses_=val;
}
void Player::Luck(int val)
{
luck_=val;
}
void Player::Wins(int val)
{
wins_=val;
}
void Player::GameBegins()
{
    shots_taken_ = duck_count_ = 0;
    ducked_ = false;
    ammo_ = 1;
}
void Player::EarnBadge(Badge b)
{
    std::cout << name_ << " is not earning " << DisplayText(b) << " because "
              << __PRETTY_FUNCTION__ << " is not overridden.\n";
}
void Player::DisplayAnimation(ani::Mation& v)
{

}
void Player::CapRating()
{
    if ( rating_ > 999 )
    {
        std::cout << name_ << "'s rating has hit the upper limit.\n";
        rating_ = 999;
    }
    else if ( rating_ < 0 )
    {
        std::cout << name_ << "'s rating has hit the lower limit\n";
        rating_ = 0;
    }
}
