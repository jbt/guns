#include "Game.hpp"
#include "Npc.hpp"
#include "Human.hpp"
#include "Ai.hpp"
#include "Color.hpp"
#include <boost/thread.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/range.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/range/adaptors.hpp>
#include <boost/lexical_cast.hpp>

using boost::asio::ip::tcp;
namespace ph = std::placeholders;

namespace {
    void RandomName( std::string& name );
    const static boost::posix_time::time_duration TIMER_INTERVAL = boost::posix_time::seconds(10);
    std::string Trim( const std::string& s );
}

Game::Game(boost::asio::io_service& service, uint16_t port, bool verbose)
: round_size_(1),
  acceptor_(service, tcp::endpoint(tcp::v4(), port)),
  players_(STATE_COUNT), verbose_(verbose), timer_ticks_(0), round_count_(0),
  timer_(service),
  moves_(), delay_(0), r_(),
  mAi( new Ai(verbose, this) ),
  cantrips_( BuildCantrips() ), cantrip_debug_(cantrips_.size())
{
    Listen();
    StartTimer();
}

void Game::Listen()
{
    if ( verbose_ )
    {
        std::cout << __PRETTY_FUNCTION__ << '(' << (void*)this << ')';
        for ( auto&& player_list : players_ )
        {
            std::cout << player_list.size() << ';';
        }
        std::cout << "\nListening for new connections." << std::endl;
    }
    if ( players_[DISCONNECTED].size() > 64 )
    {
        players_[DISCONNECTED].erase( players_[DISCONNECTED].begin() );
    }
    players_[DISCONNECTED].emplace_back( new Human{*this, acceptor_.get_io_service(), verbose_} );
    if ( delay_++ > 0 )
    {
        boost::this_thread::sleep( boost::posix_time::milliseconds(delay_) );
    }
    players_[DISCONNECTED].back()->Listen( acceptor_ );
}

void Game::PlayerAction(std::shared_ptr<Player> who, Player::Action action)
{
    if ( verbose_ )
    {
        std::cout << __func__ << '=' << who->name_ << ':' << static_cast<short>(action) << '(';
        for ( auto&& player_list : players_ )
        {
            std::cout << player_list.size() << ';';
        }
        std::cout << ")\n";
    }
    if ( who->password_.empty() && who->Wins() == 0 && delay_++ > 0 )
    {
        boost::this_thread::sleep( boost::posix_time::milliseconds(delay_) );
    }
    auto whoname = who->name_;
    std::ostringstream o;
    std::vector<std::shared_ptr<Player> > targets;
    switch ( action )
    {
        case Player::Action::CONNECT:
            if ( verbose_ ) std::cout << "CONNECT action\n";
            Listen();
            StateChange( who, DISCONNECTED, CONNECTED );
            ResetTimer();
            break;
        case Player::Action::DISCONNECT:
            for ( int i = CONNECTED; i < STATE_COUNT; ++i )
            {
                if ( StateChange(who, static_cast<PlayerState>(i), DISCONNECTED) )
                    break;
            }
            if ( ! whoname.empty() )
                Broadcast( whoname + " has disconnected." );
            who->name_.clear();
            break;
        case Player::Action::NAME:
            if ( verbose_ ) std::cout << "NAME action" << std::endl;
            Listen();
            for ( int i = CONNECTED; i < STATE_COUNT; ++i )
            {
                for ( auto p : players_[i] )
                {
                    if ( p != who && boost::iequals(p->name_, who->name_) )
                    {
                        if ( (i == THINKING || i == ACTED) && p->Connected() && p->password_.empty() )
                        {
                            p->Say("Are you still playing?");
                            RandomName( who->name_ );
                            Broadcast( std::string("A player tried to log in with the name '")+
                                    p->name_ + ", which is already taken. So instead I "
                                    "dub thee " + who->name_ );
                        }
                        else
                        {
                            StateChange( p, static_cast<Game::PlayerState>(i), DISCONNECTED );
                        }
                        i = STATE_COUNT;
                        break;
                    }
                }
            }
            Broadcast( who->name_ + " has logged on." );
            ResetTimer();
            break;
        case Player::Action::JOIN:
            if ( verbose_ ) std::cout << "join action\n";
            if ( who->argument_ == mAi->name_ )
            {
                if ( round_count_ )
                {
                    who->Say( "Not while the game is on-going." );
                }
                else if ( std::find(players_[JOINED].begin(), players_[JOINED].end(), mAi) == players_[JOINED].end() )
                {
                    players_[JOINED].push_back( mAi );
                    Broadcast(mAi->name_+" has signed up for the next round. Thanks to "+who->name_);
                }
                else
                {
                    who->Say( "He's already signed up." );
                }
            }
            else if ( StateChange(who, CONNECTED, JOINED) )
            {
                Broadcast( who->name_ + " has signed up for the next round." );
                if ( round_count_ )
                {
                    if ( verbose_ )
                        std::cout << "Round was already started.\n";
                    who->Say("Sorry but you will have to wait for this game to finish. You will be in the very next game.");
                }
                else if ( timer_ticks_ )
                {
                    ResetTimer();
                }
                else
                {
                    StartTimer();
                }
            }
            else
            {
                who->Say("Can't join - are you already signed up?");
            }
            break;
        case Player::Action::LOAD:
            if ( verbose_ ) std::cout << "RELOAD action\n";
            if ( StateChange(who, THINKING, ACTED) )
            {
//                 who->ammo_++;
//                 if ( who->ammo_ > 50 )
//                 {
//                     who->EarnBadge( Badge::READY_FOR_THE_ZOMBIES );
//                 }
                moves_.emplace_back( Move{who, Player::Action::LOAD,""} );
                if ( players_[THINKING].empty() )
                {
                    ProcessRound();
                }
                else
                {
                    ResetTimer();
                    who->Say("OK, you're loading this round.");
                }
            }
            else
            {
                who->Say("Can't load. Did you already make your move, or maybe you aren't in this round?");
            }
            break;
        case Player::Action::DUCK:
            if ( verbose_ ) std::cout << "SHIELD action\n";
            if ( StateChange(who, THINKING, ACTED) )
            {
                who->ducked_ = true;
                if ( who->duck_count_++ > 50 )
                {
                    who->EarnBadge( Badge::DUCK_DUCK_GOO_NO_DUCK );
                }
                //moves_.emplace( moves_.begin(), Action{who, Player::Action::DUCK} );
                moves_.emplace_back( Move{who, Player::Action::DUCK,""} );
                if ( players_[THINKING].empty() )
                {
                    ProcessRound();
                }
                else
                {
                    ResetTimer();
                    who->Say("OK, you're ducking this round.");
                }
            }
            else
            {
                who->Say("Can't duck. Did you already make your move, or maybe you aren't in this round?");
            }
            break;
        case Player::Action::SAY:
            Broadcast( who->name_ + " says '" + who->argument_ + "'." );
            break;
        case Player::Action::SHOOT:
            if ( verbose_ ) std::cout << "SHOOT action\n";
            if ( who->ammo_ < 1 )
            {
                who->Say("You can't shoot until you load your gun.");
            }
            else if ( StateChange(who, THINKING, ACTED) )
            {
                targets = ResolvePrefix( who->argument_ );
                if ( targets.empty() )
                {
                    who->Say( who->argument_ + " is not a player in this game. Typo?" );
                }
                else if ( targets.size() > 1 )
                {
                    auto it = boost::range::find_if( targets, [&](const std::shared_ptr<Player>&p){return boost::iequals(who->argument_,p->name_);} );
                    if ( it == targets.end() )
                    {
                        std::ostringstream msg;
                        msg << who->argument_
                            << " is not enough of a prefix to distinguish between: ";
                        for ( auto t : targets )
                        {
                            msg << t->name_ << ';';
                        }
                        who->Say( msg.str() );
                    }
                    else
                    {
                        targets.erase( targets.begin(), it );
                        targets.resize(1);
                    }
                }
                if ( targets.size() == 1 )
                {
                    if ( targets.front() == who )
                    {
                        who->Say("No suicide please.");
                        StateChange(who, ACTED, THINKING);
                        return;
                    }
//                     who->ammo_--;
                    who->shots_taken_++;
                    moves_.emplace_back( Move{who, Player::Action::SHOOT, targets.front()->name_} );
                    if ( players_[THINKING].empty() )
                    {
                        ProcessRound();
                    }
                    else
                    {
                        ResetTimer();
                        who->Say(std::string("OK, you're shooting ")+targets.front()->name_+" this round.");
                    }
                }
                else
                {
                    StateChange(who, ACTED, THINKING);
                    ResetTimer();
                }
            }
            else
            {
                who->Say("Can't shoot. Did you already make your move, or maybe you aren't in this round?");
            }
            break;
        case Player::Action::TELL:
            targets = ResolvePrefix( who->command_arguments_.at(1), true );
            if ( targets.empty() )
            {
                who->Say( who->command_arguments_.at(1) + " is not a logged-on player." );
            }
            else if ( targets.size() > 1 )
            {
                who->Say( who->command_arguments_.at(1) + " is not enough of a prefix to distinguish to whom you wish to speak." );
            }
            else
            {
                targets.front()->Say( who->name_+" whispers in your ear '"+who->argument_+"'." );
            }
            break;
        case Player::Action::WHO:
            if ( ! players_[THINKING].empty() )
            {
                o << "Waiting on: \r\n";
                for ( auto& p : players_[THINKING] )
                {
                    o << ' ' << p->name_ << "\r\n";
                }
            }
            if ( ! players_[ACTED].empty() )
            {
                o << "\nAlready made their move: \r\n";
                for ( auto& p : players_[ACTED] )
                {
                    o << ' ' << p->name_ << "\r\n";
                }
            }
            if ( ! players_[JOINED].empty() )
            {
                o << "\nWaiting for the next round: \r\n";
                for ( auto& p : players_[JOINED] )
                {
                    o << ' ' << p->name_ << "\r\n";
                }
            }
            if ( ! players_[CONNECTED].empty() )
            {
                o << "\nSpectators: \r\n";
                for ( auto& p : players_[CONNECTED] )
                {
                    o << ' ' << p->name_ << "\r\n";
                }
            }
            who->Say( o.str() );
    }
}
bool Game::StateChange(const std::shared_ptr<Player>& who, Game::PlayerState old_state, Game::PlayerState new_state)
{
    return StateChange( *who, old_state, new_state );
}
bool Game::StateChange(Player& who, Game::PlayerState old_state, Game::PlayerState new_state)
{
    auto it = std::find_if( players_[old_state].begin(), players_[old_state].end(), [&who](const std::shared_ptr<Player>&p){return p.get()==&who;} );
    if ( it == players_[old_state].end() )
    {
        if ( verbose_ )
        {
            std::cout << "Attempted to change the state of a player from " << old_state << " to "
                    << new_state << " but he was not in that state.\n";
        }
        return false;
    }
    if ( dynamic_cast<Human*>(&who) == nullptr && new_state != THINKING && new_state != ACTED )
    {
        if ( verbose_ )
            std::cout << who.name_ << " does not exist as a spectator.\n";
        players_[old_state].erase( it );
        return true;
    }
    if ( verbose_ )
        std::cout << "Player '" << who.name_ << "' transitioned from state " << old_state << " to " << new_state << '\n';
    players_[new_state].splice( players_[new_state].begin(), players_[old_state], it );
    if ( new_state == DISCONNECTED )
    {
        who.Disconnect();
    }
    return true;
}

void Game::Broadcast(const std::string& message)
{
    for ( auto&& p : Playing() )
    {
        p->Say( message );
    }
    for ( auto&& p : NotPlaying() )
    {
        p->Say( message );
    }
}

void Game::ResetTimer()
{
    if ( timer_ticks_ )
        ++timer_ticks_;
    if ( verbose_ )
        std::cout << __func__ << '=' << timer_ticks_ << '\n';
}

void Game::ProcessRound()
{
    if ( verbose_ ) std::cout << __func__ << '\n';
    std::vector<std::shared_ptr<Player> > casualties;
    if ( round_count_ )
    {
        ResetTimer();
        Broadcast(std::string("   ###   END OF ROUND ")+std::to_string(round_count_)+"!   ###   ");
        std::map<std::string,int> order;
        for ( auto&& p : players_[THINKING] )
        {
            Broadcast(p->name_+" stands motionless as if in shock.");
            order[p->name_] = moves_.size();
        }
        players_[ACTED].splice( players_[ACTED].begin(), players_[THINKING] );
        for ( int i = 0; static_cast<std::size_t>(i) < moves_.size(); ++i )
        {
            order[moves_[i].who_->name_] = i;
        }
        std::sort( moves_.begin(), moves_.end() );
        std::ostringstream o;
        for ( auto&& m : moves_ )
        {
            for ( auto& p : players_[ACTED] )
            {
                p->NotifyOfMove( m );
            }
            o << m.who_->name_;
            switch ( m.what_ )
            {
                case Player::Action::CONNECT:
                case Player::Action::DISCONNECT:
                case Player::Action::NAME:
                case Player::Action::JOIN:
                case Player::Action::SAY:
                case Player::Action::TELL:
                case Player::Action::WHO:
                    o << " does some confusing stuff (bug?).";
                    break;
                case Player::Action::LOAD:
                    m.who_->ammo_++;
                    if ( m.who_->ammo_ > 50 )
                    {
                        m.who_->EarnBadge( Badge::READY_FOR_THE_ZOMBIES );
                    }
                    o << " loads a bullet and is ready to shoot (" << m.who_->ammo_ << " bullets loaded).";
                    break;
                case Player::Action::DUCK:
                    o << " ducks in an attempt at evading the crossfire.";
                    break;
                case Player::Action::SHOOT:
                    o << " shoots at ";
                    auto target_it = std::find_if(players_[ACTED].begin(), players_[ACTED].end(),
                                                [&](const std::shared_ptr<Player>&p){return p->name_==m.target_;} );
                    if ( target_it == players_[ACTED].end() )
                    {
                        o << m.target_ << " and can't find them.";
                        continue;
                    }
                    m.who_->ammo_--;
                    auto target = *target_it;
                    Calc c;
                    Calc::ExtraResults result;
                    auto hit = c.Evaluate( order, round_count_, result, *(m.who_), *target, r_ );
                    o << target->name_ << " with a " << result.percent_ << "% chance of hitting and... ";
                    if ( hit )
                    {
                        if ( target->color_ )
                            target->DisplayAnimation( ani::SHOT() );
                        o << "HITS! " << color::RED << target->name_ << " is DEAD!"
                          << color::NONE << " 😵";
                        m.who_->ScoreKill();
                        casualties.push_back(target);
                    }
                    else
                    {
                        o << "MISSES! 😋";
                    }
                    m.who_->DeltaLuck( result.shooter_luck_mod_ );
                    target->DeltaLuck( result.target_luck_mod_ );
                    m.who_->calcs_.push_back( c );
                    target->calcs_.push_back( c );
                    break;
            }
            o << "\r\n";
        }
        Broadcast(o.str());
        std::sort( casualties.begin(), casualties.end() );
        for ( auto i = std::adjacent_find(casualties.begin(), casualties.end());
                i != casualties.end();
                    i = std::adjacent_find(std::next(i), casualties.end()) )
        {
            auto breadth = std::upper_bound( i, casualties.end(), *i );
            if ( std::distance(i, breadth) > 3 )
            {
                (*i)->EarnBadge( Badge::SWISS_CHEESE );
            }
        }
        casualties.erase( std::unique(casualties.begin(), casualties.end()), casualties.end() );
        for ( auto p : casualties )
        {
            if ( StateChange(*p, ACTED, CONNECTED) )
                p->Lose();
            if ( round_count_ == 1 && p->ducked_ )
                p->EarnBadge( Badge::DISCRETION_IS_THE_WORST_PART_OF_VALOR );
        }
        for ( auto p :  players_[ACTED] )
        {
            p->rating_ += 2 * casualties.size();
            p->CapRating();
        }
    }
    else if ( players_[JOINED].size() + players_[THINKING].size() > 1 )
    {
        players_[THINKING].splice( players_[THINKING].begin(), players_[JOINED] );
        Broadcast(color::YELLOW+"Everyone get ready... and GO!"+color::NONE);
        round_size_ = players_[THINKING].size();
        round_count_++;
        AddCantrips();
        std::vector<std::shared_ptr<Player> > ps( players_[THINKING].begin(), players_[THINKING].end() );
        for ( auto p : ps )
        {
            Post( std::bind(&Player::GameBegins,p) );
        }
        for ( auto p : ps )
        {
            Post( std::bind(&Player::RoundBegins,p) );
        }
        return;
    }
    moves_.clear();
    std::for_each( players_[ACTED].begin(), players_[ACTED].end(), [](std::shared_ptr<Player>&p){p->ducked_=false;} );
    if ( players_[ACTED].empty() )
    {
        Broadcast( "Everyone's dead. A tie!" );
        ScoreBoard();
        round_count_ = 0;
    }
    else if ( players_[ACTED].size() == 1 )
    {
        auto winner = players_[ACTED].front();
        winner->Win();
        Broadcast( winner->name_
                   + " is the only one left standing, and therefore "
                   + color::WHITE
                   + winner->name_
                   + " WINS!"+color::NONE+" 😱" );
        if ( round_count_ > 50 )
        {
            winner->EarnBadge( Badge::THE_SURVIVOR );
        }
        else if ( winner->shots_taken_ == 0 )
        {
            winner->EarnBadge( Badge::THE_PACIFIST );
        }
        StateChange( winner, ACTED, CONNECTED );
        ScoreBoard();
        round_count_ = 0;
        if ( Npc::IsStrawman(winner->name_) )
        {
            for ( auto loser : casualties )
            {
                loser->EarnBadge( Badge::THE_LION_AND_THE_TIN_MAN );
            }
        }
    }
    else
    {
        Broadcast( color::YELLOW+"Round " + std::to_string(++round_count_) + " begins." + color::NONE );
        players_[THINKING].splice( players_[THINKING].begin(), players_[ACTED] );
        AddCantrips();
        CapLuck();
        std::vector<std::shared_ptr<Player> > ps{ players_[THINKING].begin(), players_[THINKING].end() };
        if ( ps.size() > 1 )
        {
            for ( auto& p : ps )
            {
                Post( std::bind(&Player::RoundBegins,p) );
            }
        }
    }
}

void Game::StartTimer()
{
    if ( verbose_ ) std::cout << __func__ << '\n';
    ++timer_ticks_;
    timer_.expires_from_now( TIMER_INTERVAL );
    timer_.async_wait( std::bind(&Game::TimerFired, this, 0, ph::_1) );
}

void Game::TimerFired(short ticks, const boost::system::error_code& ec)
{
    timer_.expires_from_now( TIMER_INTERVAL );
    timer_.async_wait( std::bind(&Game::TimerFired, this, timer_ticks_, ph::_1) );
    if ( ec )
    {
        std::clog << __func__ << '=' << ec.message() << '\n';
        return;
    }
    delay_ /= 2;
    if ( verbose_ )
        std::cout << "delay=" << delay_ << '\n';
    if ( ticks != timer_ticks_ )
    {
        if ( verbose_ )
            std::cout << "The timer was reset.\n";
        warned_ = ready_to_start_ = false;
    }
    else if ( round_count_ )
    {
        if ( players_[ACTED].empty() )
        {
            Broadcast("Let's play!");
            warned_ = false;
        }
        else if ( warned_ || players_[THINKING].empty()
             || std::find_if(players_[THINKING].begin(), players_[THINKING].end(), [](std::shared_ptr<Player>&p){return!p->Idle();}) == players_[THINKING].end() )
        {
            warned_ = false;
            ProcessRound();
        }
        else
        {
            Broadcast("Hurry! Someone make a move in 10s or the game will move on without you.");
            warned_ = true;
        }
    }
    else if ( players_[JOINED].empty() )
    {
        if ( players_[CONNECTED].empty() )
        {
            return;
        }
        for ( auto it = players_[CONNECTED].begin(); it != players_[CONNECTED].end(); ++it )
        {
            if ( (*it)->Inactive() )
            {
                (*it)->Disconnect();
                players_[DISCONNECTED].splice( players_[DISCONNECTED].begin(),
                                               players_[CONNECTED],
                                               it );
                return;
            }
            else if ( (*it)->Idle() )
            {
                (*it)->Say("Say something so we know you're still online and don't disconnect you.\r\n");
            }
        }
    }
    else if ( players_[JOINED].size() == 1 )
    {
        auto& p = *( players_[JOINED].front() );
        if ( p.Inactive() )
        {
            p.Touch();
            p.Say("Looks like this round isn't happening. Try again some other time.");
            StateChange(p, JOINED, CONNECTED);
        }
        else
        {
            Broadcast("You need at least two players to join.");
        }
    }
    else if ( ready_to_start_ )
    {
        ready_to_start_ = false;
        if ( verbose_ )
            std::cout << "Timer starting round.\n";
        ProcessRound();
    }
    else
    {
        Broadcast("Game will start in 10s unless someone else gets involved.");
        ready_to_start_ = true;
    }
}


void Game::ScoreBoard()
{
    auto&& np = NotPlaying();
    std::vector<std::shared_ptr<Player> > ps{ np.begin(), np.end() };
    ps.push_back( mAi );
    auto longest_name_it = std::max_element( ps.begin(), ps.end(), [](const std::shared_ptr<Player>&a,const std::shared_ptr<Player>&b){return a->name_.size()<b->name_.size();} );
    auto name_width = std::max(4UL, (*longest_name_it)->name_.size());
    std::string name_field_decor( name_width, '-' );
//     std::transform( np.begin(), np.end(),
//                     std::back_inserter(ps),
//                     [](const std::shared_ptr<Player>&p){return p;} );
    std::sort( ps.begin(), ps.end(),
               [](const std::shared_ptr<Player>&a,const std::shared_ptr<Player>&b){return a->WL()>b->WL();} );
    std::ostringstream o;
    o << "-------------------------------" << name_field_decor << "----------\r\n";
    o << "RANK | WINS | LOSSES | WIN % | NAME "
      << std::string(name_width - 3, ' ')
      << "| KILLS \r\n";
    o << "-----|------|--------|-------|-" << name_field_decor << "-|-------|\r\n";
    int rank = 0;
    float last_wl = -1.f;
    for ( auto p : ps )
    {
        if ( std::abs(p->WL()-last_wl) >= 0.1 )
            ++rank;
        o << std::setw(4) << rank << " | ";
        o << std::setw(4) << p->Wins() << " | ";
        o << std::setw(6) << p->Losses() << " | ";
        o << std::setw(5) << p->WL() << " | ";
        o << p->name_ << std::string(name_width - p->name_.size(), ' ') << " | ";
        o << std::setw(5) << p->Kills() << " | ";
        o << "\r\n";
    }
    Broadcast(o.str());
}

std::vector<std::shared_ptr<Player> > Game::ResolvePrefix(const std::string& prefix, bool include_specators) const
{
    std::vector<std::shared_ptr<Player> > rv;
    if ( prefix.empty() )
        return rv;
    for ( auto&& p : Playing() )
    {
        if ( boost::istarts_with(p->name_, prefix) )
        {
            rv.push_back( p );
        }
    }
    if ( include_specators )
    {
        for ( auto&& p : NotPlaying() )
        {
            if ( boost::istarts_with(p->name_, prefix) )
            {
                rv.push_back( p );
            }
        }
    }
    std::sort( rv.begin(), rv.end(), [](const std::shared_ptr<Player>&a,const std::shared_ptr<Player>&b)
                                        {
                                            if ( a->name_.size() == b->name_.size() )
                                            {
                                                return a->name_ < b->name_;
                                            }
                                            return a->name_.size() < b->name_.size();
                                        } );
    if ( verbose_ )
    {
        std::cout << prefix << " resolves to ";
        std::ostream_iterator<const std::string> o{ std::cout,"," };
        std::transform( rv.begin(), rv.end(), o, [](const std::shared_ptr<Player>&p){return p->name_;} );
        std::cout << " when include_specators=" << std::boolalpha << include_specators << '\n';
    }
    return rv;
}
std::vector<std::shared_ptr<Player> > Game::ResolvePrefix(const std::string& prefix, bool include_specators)
{
    std::vector<std::shared_ptr<Player> > rv;
    for ( auto&& p : Playing() )
    {
        if ( boost::istarts_with(p->name_, prefix) )
        {
            rv.push_back( p );
        }
    }
    if ( include_specators )
    {
        for ( auto&& p : NotPlaying() )
        {
            if ( boost::istarts_with(p->name_, prefix) )
            {
                rv.push_back( p );
            }
        }
    }
    std::sort( rv.begin(), rv.end(), [](const std::shared_ptr<Player>&a,const std::shared_ptr<Player>&b)
                                        {
                                            if ( a->name_.size() == b->name_.size() )
                                            {
                                                return a->name_ < b->name_;
                                            }
                                            return a->name_.size() < b->name_.size();
                                        } );
    if ( verbose_ )
    {
        std::cout << prefix << " resolves to ";
        std::ostream_iterator<const std::string> o{ std::cout,"," };
        std::transform( rv.begin(), rv.end(), o, [](const std::shared_ptr<Player>&p){return p->name_;} );
        std::cout << " when include_specators=" << std::boolalpha << include_specators << '\n';
    }
    return rv;
}

void Game::AddCantrips()
{
    std::uniform_int_distribution<int> d( 0, 100 );
    for ( auto&& p : players_[THINKING] )
    {
        if ( p->cantrip_ )
            continue;
        if ( cantrip_debug_ < cantrips_.size() )
        {
            AddCantrip( *p, cantrips_.at(cantrip_debug_) );
        }
        else if ( d(r_) <= p->Luck() )
        {
            std::uniform_int_distribution<std::size_t> dist( 0, cantrips_.size() * cantrips_.size() );
            auto rndm = dist(r_);
            size_t i;
            do
            {
                for ( i = 0; i < cantrips_.size(); ++i )
                {
                    if ( verbose_ )
                        std::cout << "rndm=" << rndm << '\n';
                    if ( rndm <= i )
                        break;
                    rndm -= i;
                    if ( rndm )
                        --rndm;
                }
            } while ( i == cantrips_.size() );
            std::cout << "Selecting cantrip # " << i << std::endl;
            AddCantrip( *p, cantrips_.at(i) );
        }
    }
    ++cantrip_debug_;
}

void Game::AddCantrip(Player& p, Game::Cantrip& c) const
{
    std::ostringstream o;
    o << "You have gained a cantrip!\r\n"
        << color::BLUE
        << c.title_ << "\r\n"
        << color::NONE
        << c.description_ << "\r\n";
    p.cantrip_ = c.foo_;
    p.Say( o.str() );
}


std::vector< Game::Cantrip > Game::BuildCantrips()
{
    return
    {
        { "Robocide",
            " cantrip\r\n You shoot at every currently-playing non-human in addition to your move for the round.\r\n"
            " Note that this can easily make your ammo count go negative.\r\n Also of interest, spectators can do this.",
            [this](std::shared_ptr<Player> p)
            {
                for ( auto t : Playing() )
                {
                    if ( dynamic_cast<Human*>(t.get()) == nullptr )
                    {
                        moves_.emplace_back( Move{p, Player::Action::SHOOT, t->name_} );
                    }
                }
                return true;
            }
        },
        { "What?",
          "cantrip\r\n The output active players see will temporarily be confusing.",
          [this](std::shared_ptr<Player> p)
          {
              for ( auto& t : Playing() )
              {
                auto colored = t->color_;
                t->color_ = true;
                t->Say( "\e[4;11H" );
                t->color_ = colored;
              }
              return true;
          }
        },
        { "BEL",
          " cantrip\r\n You ring a bell.\r\n Most people will not notice your little dingaling. Some may see something, which may be subtle. The 'lucky' few may even hear something like a ding.",
          [this](std::shared_ptr<Player> p)
          {
            Broadcast("\a");
            p->Say("Ding.");
            return true;
          }
        },
        {
            "Load 'em Up",
            " cantrip \r\n You get a bonus bullet. Your opponents get two bonus bullets.",
            [this](std::shared_ptr<Player>p)
            {
                for ( auto q : Playing() )
                {
                    if ( q != p )
                    {
                        q->ammo_ += 2;
                    }
                }
                p->ammo_++;
                return true;
            }
        },
        { "Strawman",
          " cantrip\r\n A new player, who is no threat to anyone, joins the game.\r\n He won't do anything at all on the round he's summoned, because he's tired from the trip... or something.",
          [this](std::shared_ptr<Player> p)
          {
            std::shared_ptr<Npc> sm( new Npc(*this, verbose_) );
            RandomName( sm->name_ );
            sm->name_ = std::string("Strawman_") + sm->name_;
            sm->move_ = std::bind( &Npc::Strawman, sm );
            players_[ACTED].push_back( sm );
            return true;
          }
        },
        { "The Draft",
          " cantrip \r\n All current spectators who have not signed up for the next round are drafted into the next game. Also, the first (maybe only) 10-second warning is skipped, speeding up the rate at which the game starts.",
          [this](std::shared_ptr<Player>p)
          {
              if ( std::find(players_[JOINED].begin(), players_[JOINED].end(), mAi) == players_[JOINED].end() )
              {
                players_[JOINED].push_back( mAi );
              }
              for ( auto c : players_[CONNECTED] )
              {
                c->Say( "You have been drafted. Get ready!" );
              }
              players_[JOINED].splice( players_[JOINED].end(), players_[CONNECTED] );
              warned_ = true;
            return true;
          }
        },
        { "Monkey See; Monkey Shoot",
          " cantrip <name>\r\n You do whatever the named player is doing this round. This consumes your move for the round.\r\n Beware: this can cause you to shoot yourself. It can also cause your ammo count to go negative.",
          [this](std::shared_ptr<Player> p)
          {
            if ( p->command_arguments_.size() < 2 )
            {
                p->Say( "You must specify whom you're copying." );
                return false;
            }
            auto targets = ResolvePrefix(p->command_arguments_[1]);
            if ( targets.empty() )
            {
                p->Say("No one by that name is currently playing.");
                return false;
            }
            if ( targets.size() > 1 )
            {
                p->Say("More than one person by that name prefix.");
            }
            auto targ = targets.front();
            auto move = std::find_if( moves_.begin(), moves_.end(), [=](const Move&a){return a.who_==targ;} );
            if ( move == moves_.end() )
            {
                p->Say( targ->name_ + " has not made their move for this round yet." );
                return false;
            }
            if ( !StateChange(*p, THINKING, ACTED) )
            {
                p->Say("In order to use this cantrip you must be playing and not have already made your move for the round.");
                return false;
            }
            moves_.push_back( Move{p, move->what_,move->target_} );
            p->argument_ = targ->argument_;
            return true;
          }
        },
        { "Ventriloquism",
          " cantrip <name> <message>\r\n Make it look like the player named <name> said <message>.",
          [this](std::shared_ptr<Player> p)
          {
              auto&& args = p->command_arguments_;
              if ( args.size() < 3 )
              {
                  p->Say("You must specify who you want to talk, and then what you'd like them to say.");
                  return false;
              }
              auto players = ResolvePrefix( args.at(1), true );
              if ( players.empty() )
              {
                  p->Say( args.at(1) + " is not a currently-playing player." );
                  return false;
              }
              if ( players.size() > 1 )
              {
                  p->Say( args.at(1) + " is not enough to distinguish between different players." );
                  return false;
              }
              //Can't actually use PlayerAction because we don't want to mess with their target
              //Broadcast( who.name_ + " says '" + who.argument_ + "'." );
              std::ostringstream msg;
              msg << players.front()->name_ << " says '";
              static const char* words[4] = { "I", "m", "gay" };
              const char** word = words;
              for ( size_t i = 2; i < args.size(); ++i )
              {
                if ( i > 2 && ! args.at(i).empty() )
                    msg.put(' ');
                msg << Trim( args.at(i) );
                while ( word && boost::icontains(args.at(i), *word) )
                {
                    ++word;
                }
              }
              msg << "'.";
              Broadcast( Trim(msg.str()) );
              if ( !word )
                  p->EarnBadge( Badge::SPRING_CLEANING );
              return true;
        } },
        {
            "Time Warp",
            "cantrip <new round #> \r\n Set the round number to a positive whole number of your choice.",
            [this]( std::shared_ptr<Player> p )
            {
                if ( p->command_arguments_.size() < 2 )
                {
                    p->Say("You must specify the new round number");
                    return false;
                }
                try
                {
                    auto s = boost::lexical_cast<short>( p->command_arguments_[1] );
                    if ( s < 1 )
                    {
                        p->Say("The round number must be greater than zero and less than 32767.");
                    }
                    else
                    {
                        round_count_ = s;
                        return true;
                    }
                }
                catch ( const std::exception& e )
                {
                    p->Say("The round number must be a whole number (only use digits, no decimal or sign).");
                }
                return false;
            }
        }
    };
}
void Game::CheckPassword( std::shared_ptr<Player> candidate, std::shared_ptr< Player > against, const std::string& response )
{
    if ( boost::iequals(boost::trim_copy(response), boost::trim_copy(against->password_)) )
    {
        candidate->Become( *against );
        candidate->Say( std::string("Welcome. You have colors ")
                        +(candidate->color_?"en":"dis")
                        +"abled and UTF-8 "
                        +(candidate->utf8_?"en":"dis")
                        +"abled." );
        for ( int i = DISCONNECTED; i < STATE_COUNT; ++i )
        {
            if ( std::find(players_[i].begin(), players_[i].end(), against) != players_[i].end() )
            {
                StateChange( *candidate, CONNECTED, static_cast<PlayerState>(i) );
            }
        }
        against->name_.assign( "Nobody" );
        against->Disconnect();
    }
    else
    {
        candidate->Say( "Sorry, passwords did not match." );
        candidate->Disconnect();
    }
}
void Game::CapLuck()
{
    auto ps = AllPlayers();
    auto winningest = std::max_element( ps.begin(), ps.end(),
                                        [](const std::shared_ptr<Player>&a,const std::shared_ptr<Player>&b)
                                        {
                                            if ( a->Wins() != b->Wins() )
                                            return a->Wins() < b->Wins();
                                            return a->Losses() > b->Losses();
                                        } );
    auto luckiest = std::max_element( ps.begin(), ps.end(),
                                        [](const std::shared_ptr<Player>&a,const std::shared_ptr<Player>&b)
                                        {
                                        return a->Luck() < b->Luck();
                                        } );

    if ( luckiest == winningest && (*luckiest)->Luck() > 0 && (*luckiest)->Wins() > 0 )
    {
        (*luckiest)->Say( "Your luck is running out." );
        (*luckiest)->DeltaLuck( -1 );
    }
    for ( auto& p : ps )
    {
        if ( typeid(*p) != typeid(Human) )
        {
            if ( verbose_ )
                std::cout << "Ignoring the luck of " << p->name_ << " because it's not human.\n";
        }
        else if ( p->Luck() < 1 )
        {
            if ( verbose_ )
                std::cout << p->name_ << " has no luck, so no global decrement.\n";
            return;
        }
    }
    std::cout << "Reducing everyone's luck.\n";
    for ( auto& p : ps )
    {
        if ( p->Luck() > 0 )
        {
            p->Say( "Your luck is running out." );
            p->DeltaLuck( -1 );
        }
    }
}
std::vector< std::shared_ptr< Player > > Game::AllPlayers()
{
    auto p = Playing();
    std::vector< std::shared_ptr< Player > > rv{ p.begin(), p.end() };
    auto np = NotPlaying();
    rv.insert( rv.end(), np.begin(), np.end() );
    return rv;
}
void Game::Post( std::function<void()> foo )
{
    acceptor_.get_io_service().post( foo );
}

namespace {
    void RandomName( std::string& name )
    {
        name.resize(22);
        std::random_device r;
        std::uniform_int_distribution<char> d( 'A', 'Z' );
        std::generate( name.begin(), name.end(), std::bind(d,std::ref(r)) );
    }
    std::string Trim( const std::string& s )
    {
        auto first = s.find_first_not_of(" \t\r\n\v");
        auto last = s.find_last_not_of(" \t\r\n\v");
        if ( first == std::string::npos )
            return "";
        return s.substr( first, last - first + 1 );
    }
}

boost::filesystem::path DATA_DIR = ".";
