#include <boost/date_time/posix_time/posix_time.hpp>
#include <vector>
#include <string>

namespace ani
{
    typedef const std::vector<std::pair<std::string,boost::posix_time::time_duration> > Mation;
    Mation& SHOT();
}
