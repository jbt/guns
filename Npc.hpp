#ifndef NPC_HPP_INCLUDED
#define NPC_HPP_INCLUDED 1

#include "Player.hpp"

class Npc : public Player
{
public:
    Npc(Game& game, bool verbose);
    std::function<void()> move_;
    void Strawman();
    static bool IsStrawman( const std::string& name );
protected:
    virtual void RoundBegins();
    virtual void Listen(boost::asio::ip::tcp::acceptor& acceptor);
    virtual boost::asio::ip::address Address() const;
    virtual bool Connected() const;
    virtual void Disconnect();
    virtual void Say(std::string message);
    virtual void DeltaLuck( int d );
};

#endif
