#ifndef BADGE_HPP_INCLUDED
#define BADGE_HPP_INCLUDED 1

#include <iosfwd>
#include <boost/concept_check.hpp>

enum class Badge
{
    NO_BADGE,
    DISCRETION_IS_THE_WORST_PART_OF_VALOR,
    THE_LION_AND_THE_TIN_MAN,
    LUCK_O_THE_LOSERS,
    READY_FOR_THE_ZOMBIES,
    THE_BRAVE_AND_THE_PERSISTENT,
    DUCK_DUCK_GOO_NO_DUCK,
    THE_VETERAN,
    THE_SURVIVOR,
    SPRING_CLEANING,
    THE_PACIFIST,
    SWISS_CHEESE,
    EMIL_BLEEHALL,
    BADGE_MAX
    //NOTE! These badges are stored in files that outlive the binary according the value of this enum, so when adding new ones always put them at the end.
};

std::string DisplayText( Badge b );
std::ostream& operator<<( std::ostream& o, Badge b );
std::istream& operator>>( std::istream& i, Badge& b );

std::size_t BadgeCount();

#endif
