#include <string>

namespace color
{
    extern const std::string WHITE, GRAY, RED, YELLOW, BLUE, NONE, INVISIBLE;
}
