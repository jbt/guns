#include "Npc.hpp"

class Game;

class Ai : public Npc
{
public:
    Ai( bool verbose, Game* g );
private:
    Game* game_;
    int danger_, opponent_count_;
    std::map<std::string,int> grudges_;
    std::map<std::string,long> targets_;
    std::random_device r_;
    std::uniform_int_distribution<uint8_t> d_;

    virtual void GameBegins() override;
    virtual void RoundBegins() override;
    virtual void NotifyOfMove( const Move& m ) override;

    std::string Target();
    void Forgive( Player& name, int based_upon );
};
