#include "Game.hpp"
#include <boost/asio.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/filesystem.hpp>
#include <cstring>
#include <iostream>

namespace ph = std::placeholders;

int main(int argc, const char *argv[])
{
    try
    {
        boost::asio::io_service service;
        uint16_t port = 1337;
        auto b = argv + 1;
        auto e = argv + argc;
        auto last = std::next( e, -1 );
        auto arg = std::find_if_not( b, e, std::bind(std::strcmp,ph::_1,"--port") );
        if ( arg != e && arg != last )
        {
            port = boost::lexical_cast<uint16_t>( arg[1] );
        }
        auto verbose = e != std::find_if_not( b, e, std::bind(std::strcmp,ph::_1,"--verbose") );
        Game server( service, port, verbose );
        arg = std::find_if_not( b, e, std::bind(std::strcmp,ph::_1,"--cantrip-debug") );
        if ( arg != e )
        {
            if ( arg == last )
            {
                server.cantrip_debug_ = 0;
            }
            else
            {
                server.cantrip_debug_ = boost::lexical_cast<uint16_t>( arg[1] );;
            }
        }
        arg = std::find_if_not( b, e, std::bind(std::strcmp,ph::_1,"--cantrip-debug") );
        if ( arg != e && arg != last )
        {
            DATA_DIR = arg[1];
        }
        else
        {
            DATA_DIR = boost::filesystem::path(argv[0]).parent_path() / "data";
        }
        boost::filesystem::create_directory( DATA_DIR );
        service.run();
        return 0;
    }
    catch ( const std::exception& e )
    {
        std::clog << "FATAL ERROR: " << e.what() << '\n';
    }
    return 1;
}
