#ifndef MOVE_HPP_INCLUDED
#define MOVE_HPP_INCLUDED

#include "Player.hpp"

struct Move
{
    std::shared_ptr<Player> who_;
    Player::Action what_;
    std::string target_;
    bool operator<(const Move& rhs) const;
};

#endif
