#include "Npc.hpp"
#include "Game.hpp"
#include <boost/algorithm/string.hpp>

namespace {
    void thro()
    {
        throw std::string("NPCs shouldn't be calling the callback.");
    }
}

Npc::Npc(Game& game, bool verbose)
: Player(game, verbose)
{

}


void Npc::Listen(boost::asio::ip::tcp::acceptor& acceptor)
{
    if ( verbose_ ) std::cout << __PRETTY_FUNCTION__ << " - noop\n";
}

void Npc::RoundBegins()
{
    if ( verbose_ ) std::cout << __PRETTY_FUNCTION__ << " - noop\n";
    game_.Post( move_ );
}
void Npc::Disconnect()
{
    if ( verbose_ ) std::cout << __PRETTY_FUNCTION__ << " - noop\n";
}
boost::asio::ip::address Npc::Address() const
{
    return boost::asio::ip::address_v4::from_string("127.0.0.1");
}
bool Npc::Connected() const
{
    throw std::runtime_error("I never was connected.");
}
void Npc::Say(std::string message)
{
    if ( verbose_ ) std::cout << __PRETTY_FUNCTION__ << '=' << message << '\n';
}
void Npc::DeltaLuck(int d)
{
    luck_ += d;
    if ( luck_ > 0 )
        luck_ = 0;
}

void Npc::Strawman()
{
    if ( ammo_ % 9 == 8 )
    {
        auto tmp = game_.Playing();
        std::vector<std::shared_ptr<Player> > ps( tmp.begin(), tmp.end() );
        for ( auto p : ps )
        {
            if ( p.get() != this && IsStrawman(p->name_) )
            {
                argument_ = p->name_;
                game_.PlayerAction( shared_from_this(), Action::SHOOT );
            }
        }
    }
    game_.PlayerAction( shared_from_this(), Action::LOAD );
}

bool Npc::IsStrawman(const std::string& name)
{
    return boost::starts_with(name,"Strawman");
}

